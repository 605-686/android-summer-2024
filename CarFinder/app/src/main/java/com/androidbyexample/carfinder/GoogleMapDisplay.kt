package com.androidbyexample.carfinder

import android.location.Location
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.CameraPositionState
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MarkerInfoWindowContent
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberMarkerState
import kotlinx.coroutines.launch

@Composable
fun GoogleMapDisplay(
    cameraPositionState: CameraPositionState,
    currentLocation: Location?,
    carLatLng: LatLng?,
    onMoveCar: (LatLng) -> Unit,
    modifier: Modifier,
) {
    with(LocalDensity.current) {
        val boundsPadding = 48.dp.toPx()
        var mapLoaded by remember { mutableStateOf(false) }

        val currentLocationState = remember(currentLocation) {
            currentLocation?.let {
                MarkerState(
                    LatLng(
                        it.latitude,
                        it.longitude
                    )
                )
            }
        }

        var currentMapType by remember {
            mutableStateOf(MapType.NORMAL)
        }
        val mapProperties by remember(currentMapType) {
            mutableStateOf(MapProperties(mapType = currentMapType))
        }
        val scope = rememberCoroutineScope()
        val context = LocalContext.current
        var currentLocationIcon by remember {
            mutableStateOf<BitmapDescriptor?>(null)
        }
        var carIcon by remember {
            mutableStateOf<BitmapDescriptor?>(null)
        }
        val carState = rememberMarkerState("car")

        var initialBoundsSet by remember {
            mutableStateOf(false)
        }

        LaunchedEffect(key1 = currentLocation, key2 = mapLoaded) {
            if (!initialBoundsSet && mapLoaded && currentLocation != null) {
                initialBoundsSet = true
                val current =
                    LatLng(currentLocation.latitude, currentLocation.longitude)
                carLatLng?.let { car ->
                    val bounds =
                        LatLngBounds(current, current).including(car)
//                    var bounds = LatLngBounds(current, current)
//                    bounds = bounds.including(car)
                    cameraPositionState.animate(
                        CameraUpdateFactory.newLatLngBounds(
                            bounds,
                            boundsPadding.toInt()
                        ), 1000
                    )
                } ?: run {
                    cameraPositionState.animate(
                        CameraUpdateFactory.newLatLngZoom(
                            current,
                            16f
                        ), 1000
                    )
                }
            }
        }

        LaunchedEffect(true) {
            var dragged = false
            snapshotFlow { carState.isDragging }
                .collect { dragging ->
                    // Make sure we've seen at least one drag state before updating
                    //   the view model. Otherwise we'll see the initial (0.0, 0.0)
                    //   value that was set when the MarkerState was created
                    if (dragging) {
                        dragged = true
                    } else {
                        if (dragged) {
                            onMoveCar(carState.position)
                            dragged = false
                        }
                    }
                }
        }


        Box(
            modifier = modifier
        ) {
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                MapTypeSelector(
                    currentValue = currentMapType,
                    onMapTypeClick = { currentMapType = it },
                    modifier = Modifier.fillMaxWidth(),
                )
                GoogleMap(
                    cameraPositionState = cameraPositionState,
                    properties = mapProperties,
                    onMapLoaded = {
                        scope.launch {
                            currentLocationIcon =
                                context.loadBitmapDescriptor(R.drawable.ic_current_location)
                            carIcon =
                                context.loadBitmapDescriptor(R.drawable.ic_car)
                            mapLoaded = true
                        }
                    },
                    modifier = Modifier.weight(1f)
                ) {
                    currentLocationState?.let { markerState ->
                        currentLocationIcon?.let { icon ->
                            MarkerInfoWindowContent(
                                state = markerState,
                                icon = icon,
                                title = stringResource(id = R.string.current_location),
                                anchor = Offset(0.5f, 0.5f)
                            )
                        }
                    }
                    carLatLng?.let { car ->
                        carIcon?.let { icon ->
                            carState.position = car
                            MarkerInfoWindowContent(
                                state = carState,
                                draggable = true,
                                icon = icon,
                                title = stringResource(id = R.string.car_location),
                                anchor = Offset(0.5f, 0.5f)
                            )
                        }
                    }
                }
            }

            if (!mapLoaded) {
                AnimatedVisibility(
                    visible = true,
                    modifier = Modifier.fillMaxSize(),
                    enter = EnterTransition.None,
                    exit = fadeOut()
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .background(MaterialTheme.colorScheme.background)
                            .wrapContentSize()
                    )
                }
            }
        }
    }
}
