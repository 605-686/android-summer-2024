package com.androidbyexample.carfinder

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.core.app.ActivityCompat
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.carfinder.ui.theme.CarFinderTheme
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.rememberCameraPositionState

private val googleHQ = LatLng(37.42423291057923, -122.08811454627153)
private val defaultCameraPosition = CameraPosition.fromLatLngZoom(googleHQ, 11f)

class MainActivity : ComponentActivity() {
    private val viewModel: CarViewModel by viewModels()

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            viewModel.updateLocation(locationResult.lastLocation)
        }
    }

    private val getLocationPermission =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { isGranted ->
            if (isGranted.values.any { it }) {
                startLocationAndMap()
            } else {
                // if the user denied permissions, tell them they
                //   cannot use the app without them. In general,
                //   you should try to just reduce function and let the
                //   user continue, but location is a key part of this
                //   application.
                //   (Note that a real version of this application
                //   might allow the user to manually click on the map
                //   to set their current location, and we wouldn't
                //   show this dialog, or perhaps only show it once)
                // NOTE: This is a normal Android-View-based dialog, not a compose one!
                AlertDialog.Builder(this)
                    .setTitle("Permissions Needed")
                    .setMessage(
                        "We need coarse-location or fine-location permission " +
                                "to locate a car (fine location is highly " +
                                "recommended for accurate car locating). " +
                                "Please allow these permissions via App Info " +
                                "settings")
                    .setCancelable(false)
                    .setNegativeButton("Quit") { _, _ -> finish() }
                    .setPositiveButton("App Info") { _, _ ->
                        startActivity(
                            Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            ).apply {
                                data = Uri.parse("package:$packageName")
                            }
                        )
                        finish()
                    }
                    .show()
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GoogleApiAvailability.getInstance()
            .makeGooglePlayServicesAvailable(this)
            .addOnSuccessListener {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    getLocationPermission.launch(
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                    )
                } else {
                    startLocationAndMap()
                }
            }
            .addOnFailureListener(this) {
                Toast.makeText(
                    this,
                    "Google Play services required (or upgrade required)",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationAndMap() {
        val locationRequest =
            LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 5000)
                .setWaitForAccurateLocation(false)
                .setMinUpdateIntervalMillis(0)
                .setMaxUpdateDelayMillis(5000)
                .build()
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )

        enableEdgeToEdge()
        setContent {
            CarFinderTheme {
                val currentLocation by viewModel.currentLocation.collectAsStateWithLifecycle(
                    initialValue = null
                )
                val carLatLng by
                    viewModel.carLatLng.collectAsStateWithLifecycle(initialValue = null)
                val context = LocalContext.current

                Scaffold(
                    topBar = {
                        CarTopBar(
                            currentLocation = currentLocation,
                            carLatLng = carLatLng,
                            onSetCarLocation = viewModel::setCarLocation,
                            onGoToCurrentLocation = { /*TODO*/ },
                            onClearCarLocation = viewModel::clearCarLocation,
                            onWalkToCar = {
                                currentLocation?.let { curr ->
                                    carLatLng?.let { car ->
                                        val uri =
                                            Uri.parse(
                                                "https://www.google.com/maps/dir/" +
                                                        "?api=1&origin=${curr.latitude}," +
                                                        "${curr.longitude}&" +
                                                        "destination=${car.latitude}," +
                                                        "${car.longitude}&travelmode=walking")
                                        context.startActivity(
                                            Intent(
                                                Intent.ACTION_VIEW,
                                                uri
                                            ).apply {
                                                setPackage("com.google.android.apps.maps")
                                            })
                                    } ?: Toast.makeText(
                                        context,
                                        "Cannot navigate; no car location available",
                                        Toast.LENGTH_LONG
                                    ).show()
                                } ?: Toast.makeText(
                                    context,
                                    "Cannot navigate; no current location available",
                                    Toast.LENGTH_LONG
                                ).show()
                            },                        )
                    },
                    modifier = Modifier.fillMaxSize()
                ) { innerPadding ->
                    val cameraPositionState = rememberCameraPositionState {
                        position = defaultCameraPosition
                    }
                    GoogleMapDisplay(
                        currentLocation = currentLocation,
                        cameraPositionState = cameraPositionState,
                        carLatLng = carLatLng,
                        onMoveCar = viewModel::setCarLocation,
                        modifier = Modifier.padding(innerPadding)
                    )
                }
            }
        }
    }
}
