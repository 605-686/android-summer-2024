package com.androidbyexample.composegauge

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun Gauge(
    outlineColor: Color,
    fillColor: Color,
    outlineWidth: Dp,
    value: Float,
    modifier: Modifier,
) {
    with(LocalDensity.current) {
        val outlineWidthPx = outlineWidth.toPx()
        val outlineStroke = remember(outlineWidthPx) {
            Stroke(outlineWidthPx)
        }
        Canvas(modifier = modifier.padding(8.dp)) {
            val fillHeight = size.height * value
            val offsetY = size.height - fillHeight
            drawRect(
                color = fillColor,
                topLeft = Offset(0f, offsetY),
                size = Size(size.width, fillHeight)
            )
            drawRect(
                color = outlineColor,
                topLeft = Offset.Zero, // Offset(0f, 0f)
                size = Size(size.width, size.height),
                style = outlineStroke
            )
        }
    }
}
