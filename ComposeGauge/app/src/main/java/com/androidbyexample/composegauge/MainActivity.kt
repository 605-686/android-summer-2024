package com.androidbyexample.composegauge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.min
import com.androidbyexample.composegauge.ui.theme.ComposeGaugeTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            ComposeGaugeTheme {
                Scaffold { paddingValues ->
                    var value by remember {
                        mutableFloatStateOf(0f)
                    }
                    var fillColor by remember {
                        mutableStateOf(Color.Black)
                    }

                    var hour by remember { mutableIntStateOf(0) }
                    var minute by remember { mutableIntStateOf(0) }

                    LaunchedEffect(true) {
                        while(true) {
                            if (minute + 1 > 59) {
                                hour++
                                minute = 0
                            } else {
                                minute++
                            }
                            delay(10)
                        }
                    }

                    LaunchedEffect(true) {
                        var colorNum = 0
                        while(true) {
                            value += 0.1f
                            value = value.coerceIn(0f, 1f)
                            withContext(Dispatchers.Main) {
                                fillColor =
                                    when (colorNum) {
                                        0 -> Color.Blue
                                        1 -> Color.Green
                                        else -> Color.Red
                                    }
                            }
                            colorNum = (colorNum+1) % 3
                            delay(1000)
                        }
                    }

                    Column(modifier = Modifier.padding(paddingValues).fillMaxSize()) {
                        Gauge(
                            outlineColor = Color.Black,
                            fillColor = fillColor,
                            outlineWidth = 8.dp,
                            value = value,
                            modifier = Modifier.weight(0.5f).fillMaxWidth()
                        )
                        Clock(
                            hour = hour,
                            minute = minute,
                            outlineWidth = 8.dp,
                            modifier = Modifier.weight(0.5f).fillMaxWidth()
                        )
                    }
                }
            }
        }
    }
}

