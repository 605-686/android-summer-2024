package com.androidbyexample.composegraph

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput

@Composable
fun Graph(
    shapeAttributes: ShapeAttributes,
    shapes: List<Shape>,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    onAddShape: (Shape) -> Unit,
    modifier: Modifier = Modifier
) {
    Scaffold(
        topBar = {
            GraphTopBar(
                shapeAttributes = shapeAttributes,
                selectedTool = selectedTool,
                triangleColor = Color.Red,
                onToolChange = onToolChange,
            )
        },
        modifier = modifier,
    ) { paddingValues ->
        val baseModifier = Modifier.padding(paddingValues).fillMaxSize()
        val clickModifier = remember(selectedTool, paddingValues, shapeAttributes) {
            when (selectedTool) {
                DrawLine ->
                    baseModifier.pointerInput(shapeAttributes) {
                        TODO()
                    }
                Select ->
                    baseModifier.pointerInput(shapeAttributes) {
                        TODO()
                    }
                is ShapeType ->
                    baseModifier.pointerInput(shapeAttributes, selectedTool) {
                        detectTapGestures(
                            onTap = {
                                onAddShape(
                                    Shape(
                                        shapeType = selectedTool,
                                        offset = it - shapeAttributes.halfBoxOffset
                                    )
                                )
                            }
                        )
                    }
            }
        }

        Canvas(
            modifier = clickModifier
        ) {
            shapes.forEach {
                val outlineColor =
                //                    if (highlightShapeType == it.shapeType)
                //                        highlightedBorderColor
                    //                    else
                    shapeAttributes.normalOutline

                when(it.shapeType) {
                    Circle -> drawCircle(shapeAttributes, it.offset.x, it.offset.y)
                    Square -> drawSquare(shapeAttributes, it.offset.x, it.offset.y, true)
                    Triangle -> drawPath(shapeAttributes, it.offset.x, it.offset.y, shapeAttributes.trianglePath)
                }
            }
        }
    }
}


// Note on composition locals
//    please - don't create your own!!!

//comp local "LocalDensity" - defines a "Density"
//    density for current screen

//Scaffold
//        Column
//            Row
//                Define a new value for LocalDensity
//                      Graph
//            Clock