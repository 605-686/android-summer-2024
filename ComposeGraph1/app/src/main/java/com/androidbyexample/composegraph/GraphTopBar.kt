package com.androidbyexample.composegraph

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GraphTopBar(
    shapeAttributes: ShapeAttributes,
    selectedTool: ToolType,
    triangleColor: Color,
    onToolChange: (ToolType) -> Unit,
) {
    TopAppBar(
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primary,
        ),
        title = {
            Text(text = stringResource(id = R.string.graphs))
        },
        actions = {
            ToolbarButton(
                toolType = Square,
                shapeBoxSize = shapeAttributes.shapeBoxSize,
                selectedTool = selectedTool,
                onToolChange = onToolChange,
                draw = { x, y ->
                    drawSquare(
                        shapeAttributes,
                        x,
                        y,
                        true,
                    )
                }
            )
            ToolbarButton(
                toolType = Circle,
                shapeBoxSize = shapeAttributes.shapeBoxSize,
                selectedTool = selectedTool,
                onToolChange = onToolChange,
                draw = { x, y -> drawCircle(shapeAttributes, x, y) }
            )
            ToolbarButton(
                toolType = Triangle,
                shapeBoxSize = shapeAttributes.shapeBoxSize,
                selectedTool = selectedTool,
                onToolChange = onToolChange,
                draw = { x, y -> drawPath(shapeAttributes, x, y, shapeAttributes.trianglePath, triangleColor) }
            )
            ToolbarButton(
                toolType = DrawLine,
                shapeBoxSize = shapeAttributes.shapeBoxSize,
                selectedTool = selectedTool,
                onToolChange = onToolChange,
                draw = { x, y -> drawLine(shapeAttributes, x, y) }
            )
            ToolbarButton(
                toolType = Select,
                shapeBoxSize = shapeAttributes.shapeBoxSize,
                selectedTool = selectedTool,
                onToolChange = onToolChange,
                draw = { x, y ->
                    drawSquare(
                        shapeAttributes,
                        x,
                        y,
                        fill = false,
                    )
                }
            )
        }
    )
}

@Composable
fun ToolbarButton(
    toolType: ToolType,
    shapeBoxSize: Size,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    draw: DrawScope.(Float, Float) -> Unit
) {
    val backgroundColor = MaterialTheme.colorScheme.onPrimary
    Canvas(
        modifier = Modifier
            .size(48.dp)
            .clickable { onToolChange(toolType) }
    ) {
        if (selectedTool == toolType) {
            drawRect(color = backgroundColor, topLeft = Offset.Zero, size = shapeBoxSize)
        }
        draw(0f,0f)
    }
}