package com.androidbyexample.composegraph

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

class GraphViewModel: ViewModel() {
    private val _shapes = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: Flow<List<Shape>>
        get() = _shapes

    private val _selectedTool = MutableStateFlow<ToolType>(Square)
    val selectedTool: Flow<ToolType>
        get() = _selectedTool

    fun addShape(shape: Shape) {
        _shapes.value += shape
    }

    fun select(tool: ToolType) {
        _selectedTool.value = tool
    }
}