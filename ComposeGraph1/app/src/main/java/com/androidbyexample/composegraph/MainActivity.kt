package com.androidbyexample.composegraph

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.composegraph.ui.theme.ComposeGraphTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GraphViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            with(LocalDensity.current) {
                val shapeAttributes = remember {
                    ShapeAttributes(
                        density = this,
                        shapeBoxWidth = 48.dp,
                        shapeWidth = 36.dp,
                        strokeWidth = 3.dp,
                        dashLength = 6.dp,
                        dashGap = 3.dp,
                        circleColor = Color.Green,
                        squareColor = Color.Blue,
                        triangleColor = Color.Red,
                        outlineColor = Color.Black,
                    )
                }

                val selectedTool by viewModel.selectedTool.collectAsStateWithLifecycle(initialValue = Square)
                val shapes by viewModel.shapes.collectAsStateWithLifecycle(initialValue = emptyList())

                ComposeGraphTheme {
                    Graph(
                        shapeAttributes = shapeAttributes,
                        shapes = shapes,
                        selectedTool = selectedTool,
                        onToolChange = viewModel::select,
                        onAddShape = viewModel::addShape,
                        modifier = Modifier.fillMaxSize()
                    )
                }

            }
        }
    }
}
