package com.androidbyexample.composegraph

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp

class ShapeAttributes(
    density: Density,
    val shapeBoxWidth: Dp, // box surrounding a shape
    val shapeWidth: Dp, // the size of the shape itself
    val strokeWidth: Dp, // the width of our outlines
    val dashLength: Dp, // the length of dashes
    val dashGap: Dp, // the gape between dashes
    val circleColor: Color,
    val squareColor: Color,
    val triangleColor: Color,
    val outlineColor: Color,
) {
    val shapeBoxSize: Size
    val shapeSize: Size
    val halfBoxOffset: Offset
    val shapeBoxSizePx: Float
    val shapeOffsetPx: Float
    val shapeSizePx: Float
    val strokeWidthPx: Float
    val dashLengthPx: Float
    val dashGapPx: Float
    val radiusPx: Float

    val dashedOutline: Stroke
    val normalOutline: Stroke

    val trianglePath: Path

    init {
        with(density) {
            shapeBoxSize = Size(shapeBoxWidth.toPx(), shapeBoxWidth.toPx())
            shapeSize = Size(shapeWidth.toPx(), shapeWidth.toPx())
            shapeSizePx = shapeWidth.toPx()
            shapeBoxSizePx = shapeBoxWidth.toPx()
            shapeOffsetPx = (shapeBoxSizePx - shapeSizePx) / 2
            halfBoxOffset = Offset(shapeBoxSizePx/2, shapeBoxSizePx/2)
            radiusPx = shapeSizePx / 2
            strokeWidthPx = strokeWidth.toPx()
            dashLengthPx = dashLength.toPx()
            dashGapPx = dashGap.toPx()
            dashedOutline = Stroke(
                strokeWidthPx,
                pathEffect = PathEffect.dashPathEffect(floatArrayOf(dashLengthPx, dashGapPx))
            )
            normalOutline = Stroke(strokeWidthPx)
            trianglePath =
                Path().apply {
                    moveTo(shapeSizePx/2, 0f)
                    lineTo(shapeSizePx, shapeSizePx)
                    lineTo(0f, shapeSizePx)
                    close()
                }
        }
    }
}