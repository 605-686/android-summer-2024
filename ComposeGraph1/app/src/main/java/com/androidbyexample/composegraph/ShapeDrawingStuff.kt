package com.androidbyexample.composegraph

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.translate

// note on shapeoffset
// when you tap on an item to drag it, your finger is probably not at the middle or a corner
//  of that shape


fun DrawScope.drawPath(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
    path: Path,
    fillColor: Color = shapeAttributes.triangleColor,
) {
    translate(x + shapeAttributes.shapeOffsetPx, y + shapeAttributes.shapeOffsetPx) {
        drawPath(path = path, color = fillColor)
        drawPath(path = path, color = shapeAttributes.outlineColor, style = shapeAttributes.normalOutline)
    }
}

fun DrawScope.drawSquare(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
    fill: Boolean = true,
) {
    translate(x + shapeAttributes.shapeOffsetPx, y + shapeAttributes.shapeOffsetPx) {
        if (fill) {
            drawRect(color = shapeAttributes.squareColor, topLeft = Offset.Zero, size = shapeAttributes.shapeSize, style = Fill)
        }
        drawRect(color = shapeAttributes.outlineColor, topLeft = Offset.Zero, size = shapeAttributes.shapeSize, style = shapeAttributes.normalOutline)
    }
}
fun DrawScope.drawCircle(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
) {
    translate(x + shapeAttributes.shapeOffsetPx, y + shapeAttributes.shapeOffsetPx) {
        val center = Offset(shapeAttributes.shapeSizePx/2, shapeAttributes.shapeSizePx/2)
        drawCircle(color = shapeAttributes.circleColor, center = center, radius = shapeAttributes.radiusPx, style = Fill)
        drawCircle(color = shapeAttributes.outlineColor, center = center, radius = shapeAttributes.radiusPx, style = shapeAttributes.normalOutline)
    }
}
fun DrawScope.drawLine(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
) {
    translate(x, y) {
        val lineY = shapeAttributes.shapeBoxSizePx/2
        drawLine(
            color = shapeAttributes.outlineColor,
            strokeWidth = shapeAttributes.strokeWidthPx,
            start = Offset(shapeAttributes.shapeOffsetPx, lineY),
            end = Offset(shapeAttributes.shapeBoxSizePx - shapeAttributes.shapeOffsetPx, lineY)
        )
    }
}