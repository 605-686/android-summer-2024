package com.androidbyexample.composegraph

import androidx.compose.ui.geometry.Offset
import java.util.UUID

sealed interface ToolType
data object DrawLine: ToolType
data object Select: ToolType

sealed interface ShapeType: ToolType
data object Circle: ShapeType
data object Square: ShapeType
data object Triangle: ShapeType

data class Shape(
    val id: String = UUID.randomUUID().toString(),
    val shapeType: ShapeType,
    val offset: Offset,
)
