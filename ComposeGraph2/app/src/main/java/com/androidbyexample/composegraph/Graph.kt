package com.androidbyexample.composegraph

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput

@Composable
fun Graph(
    shapeAttributes: ShapeAttributes,
    shapes: List<Shape>,
    lines: List<Line>,
    highlightShapeType: ShapeType?,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    onAddShape: (Shape) -> Unit,
    onHighlightShape: (Offset, Float) -> Unit,
    onPress: (Offset) -> Unit,
    onDragStart: (Offset, Float) -> Unit,
    onDrag: (Offset) -> Unit,
    onDragEnd: () -> Unit,
    onLineStart: (Offset, Float) -> Unit,
    onLineEnd: (Offset, Float) -> Unit,
    modifier: Modifier = Modifier
) {
    fun List<Shape>.findWithId(id: String) =
        firstOrNull { it.id == id } ?:
            throw IllegalStateException("Internal error: Id $id not found for any shapes")

    fun Shape.getCenter() = offset + shapeAttributes.halfBoxOffset

    Scaffold(
        topBar = {
            GraphTopBar(
                shapeAttributes = shapeAttributes,
                selectedTool = selectedTool,
                triangleColor = Color.Red,
                onToolChange = onToolChange,
            )
        },
        modifier = modifier,
    ) { paddingValues ->
        // blinking
        //   - figure out which type of shape we clicked
        //   - loop through all shapes and change

        var lineFinger by remember {
            mutableStateOf(Offset.Zero)
        }

        val baseModifier = Modifier
            .padding(paddingValues)
            .fillMaxSize()
        val clickModifier = remember(selectedTool, paddingValues, shapeAttributes) {
            when (selectedTool) {
                DrawLine ->
                    baseModifier.pointerInput(shapeAttributes, selectedTool) {
                        detectDragGestures(
                            onDragStart = { offset ->
                                onLineStart(offset, shapeAttributes.shapeBoxSizePx)
                            },
                            onDrag = { change, _ ->
                                lineFinger = change.position
                            },
                            onDragEnd = {
                                onLineEnd(lineFinger, shapeAttributes.shapeBoxSizePx)
                            },
                            onDragCancel = {
                                onLineEnd(lineFinger, shapeAttributes.shapeBoxSizePx)
                            }
                        )
                    }
                Select ->
                baseModifier
                    .pointerInput(shapeAttributes, selectedTool) {
                        detectTapGestures(
                            onTap = { offset ->
                                onHighlightShape(offset, shapeAttributes.shapeBoxSizePx)
                            },
                            onPress = { offset ->
                                onPress(offset)
                            }
                        )
                    }
                    .pointerInput(shapeAttributes, selectedTool) {
                        detectDragGestures(
                            onDragStart = { offset ->
                                onDragStart(offset, shapeAttributes.shapeBoxSizePx)
                            },
                            onDrag = { change, _ ->
                                onDrag(change.position)
                            },
                            onDragEnd = {
                                onDragEnd()
                            },
                            onDragCancel = {
                                onDragEnd()
                            },
                        )
                    }
                is ShapeType ->
                    baseModifier.pointerInput(shapeAttributes, selectedTool) {
                        detectTapGestures(
                            onTap = {
                                onAddShape(
                                    Shape(
                                        shapeType = selectedTool,
                                        offset = it - shapeAttributes.halfBoxOffset
                                    )
                                )
                            }
                        )
                    }
            }
        }

        Canvas(
            modifier = clickModifier
        ) {
            lines.forEach { line ->
                drawLine(
                    color = shapeAttributes.outlineColor,
                    strokeWidth = shapeAttributes.strokeWidthPx,
                    start = shapes.findWithId(line.shape1Id).getCenter(),
                    end = line.shape2Id?.let { shapes.findWithId(it).getCenter() } ?: lineFinger
                )
            }
            shapes.forEach {
                val outlineColor =
                    if (highlightShapeType == it.shapeType)
                        shapeAttributes.highlightedBorderColor
                    else
                        shapeAttributes.outlineColor

                when(it.shapeType) {
                    Circle -> drawCircle(shapeAttributes, it.offset.x, it.offset.y, outlineColor)
                    Square -> drawSquare(shapeAttributes, it.offset.x, it.offset.y, outlineColor, fill = true)
                    Triangle -> drawPath(shapeAttributes, it.offset.x, it.offset.y, shapeAttributes.trianglePath, outlineColor)
                }
            }
        }
    }
}


// Note on composition locals
//    please - don't create your own!!!

//comp local "LocalDensity" - defines a "Density"
//    density for current screen

//Scaffold
//        Column
//            Row
//                Define a new value for LocalDensity
//                      Graph
//            Clock