package com.androidbyexample.composegraph

import androidx.compose.ui.geometry.Offset
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class GraphViewModel: ViewModel() {
    private val _highlightShapeType = MutableStateFlow<ShapeType?>(null)
    val highlightShapeType: Flow<ShapeType?>
        get() = _highlightShapeType

    private val _shapes = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: Flow<List<Shape>>
        get() = _shapes

    private val _lines = MutableStateFlow<List<Line>>(emptyList())
    val lines: Flow<List<Line>>
        get() = _lines

    private val _selectedTool = MutableStateFlow<ToolType>(Square)
    val selectedTool: Flow<ToolType>
        get() = _selectedTool

    fun addShape(shape: Shape) {
        _shapes.value += shape
    }

    fun select(tool: ToolType) {
        _selectedTool.value = tool
    }

    fun highlightShape(offset: Offset, shapeBoxSizePx: Float) {
        viewModelScope.launch(Dispatchers.Default) {
            findAt(offset, shapeBoxSizePx)?.let {  shape ->
                repeat(3) {
                    _highlightShapeType.value = shape.shapeType
                    delay(200)
                    _highlightShapeType.value = null
                    if (it != 2) {
                        delay(200)
                    }
                }
            }
        }
    }

    private var dragShape: Shape? = null
    private var dragShapeOffset: Offset = Offset.Zero
    private var initialPressOffset: Offset = Offset.Zero

    fun press(offset: Offset) {
        initialPressOffset = offset
    }

    fun dragStart(offset: Offset, shapeBoxSizePx: Float) {
        dragShape = findAt(initialPressOffset, shapeBoxSizePx)?.apply {
            dragShapeOffset = initialPressOffset - this.offset
            drag(offset)
        }
    }

    fun drag(offset: Offset) {
        dragShape?.let { shape ->
            val newShape = shape.copy(offset = offset - dragShapeOffset)
            _shapes.value = _shapes.value - shape + newShape
            dragShape = newShape
        }
    }

    fun dragEnd() {
        dragShape = null
    }

    private var lineInProgress: Line? = null
    fun startLine(offset: Offset, shapeBoxSizePx: Float) {
        findAt(offset, shapeBoxSizePx)?.let { shape ->
            lineInProgress = Line(shape.id).apply {
                _lines.value += this
            }
// equivalent to
//            val newLine = Line(shape.id)
//            _lines.value += newLine
//            lineInProgress = newLine
        }
    }

    fun endLine(offset: Offset, shapeBoxSizePx: Float) {
        lineInProgress?.let { line ->
            findAt(offset, shapeBoxSizePx)?.let { endShape ->
                _lines.value = _lines.value - line + line.copy(shape2Id = endShape.id)
            } ?: run {
                _lines.value -= line
            }
            lineInProgress = null
        }
    }

    private fun findAt(offset: Offset, shapeBoxSizePx: Float) =
        _shapes.value.asReversed().find { shape ->
            val normalized = offset - shape.offset
            normalized.x >= 0 &&
                normalized.y >= 0 &&
                normalized.x <= shapeBoxSizePx &&
                normalized.y <= shapeBoxSizePx
// without normalizing
//            normalized.x >= shape.offset.x &&
//                normalized.y >= shape.offset.y &&
//                normalized.x <= shape.offset.x + shapeBoxSizePx &&
//                normalized.y <= shape.offset.y + shapeBoxSizePx
        }
}