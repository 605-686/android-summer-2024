package com.androidbyexample.composegraph

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate

// note on shapeoffset
// when you tap on an item to drag it, your finger is probably not at the middle or a corner
//  of that shape


fun DrawScope.drawPath(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
    path: Path,
    outlineColor: Color = shapeAttributes.outlineColor,
    fillColor: Color = shapeAttributes.triangleColor,
) {
    translate(x + shapeAttributes.shapeOffsetPx, y + shapeAttributes.shapeOffsetPx) {
        drawPath(path = path, color = fillColor)
        drawPath(path = path, color = outlineColor, style = shapeAttributes.normalOutline)
    }
}

fun DrawScope.drawSquare(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
    outlineColor: Color = shapeAttributes.outlineColor,
    outline: Stroke = shapeAttributes.normalOutline,
    fill: Boolean = true,
) {
    translate(x + shapeAttributes.shapeOffsetPx, y + shapeAttributes.shapeOffsetPx) {
        if (fill) {
            drawRect(color = shapeAttributes.squareColor, topLeft = Offset.Zero, size = shapeAttributes.shapeSize, style = Fill)
        }
        drawRect(color = outlineColor, topLeft = Offset.Zero, size = shapeAttributes.shapeSize, style = outline)
    }
}
fun DrawScope.drawCircle(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
    outlineColor: Color = shapeAttributes.outlineColor,
) {
    translate(x + shapeAttributes.shapeOffsetPx, y + shapeAttributes.shapeOffsetPx) {
        val center = Offset(shapeAttributes.shapeSizePx/2, shapeAttributes.shapeSizePx/2)
        drawCircle(color = shapeAttributes.circleColor, center = center, radius = shapeAttributes.radiusPx, style = Fill)
        drawCircle(color = outlineColor, center = center, radius = shapeAttributes.radiusPx, style = shapeAttributes.normalOutline)
    }
}
fun DrawScope.drawLine(
    shapeAttributes: ShapeAttributes,
    x: Float,
    y: Float,
) {
    translate(x, y) {
        val lineY = shapeAttributes.shapeBoxSizePx/2
        drawLine(
            color = shapeAttributes.outlineColor,
            strokeWidth = shapeAttributes.strokeWidthPx,
            start = Offset(shapeAttributes.shapeOffsetPx, lineY),
            end = Offset(shapeAttributes.shapeBoxSizePx - shapeAttributes.shapeOffsetPx, lineY)
        )
    }
}