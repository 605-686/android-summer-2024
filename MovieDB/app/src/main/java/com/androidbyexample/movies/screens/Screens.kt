package com.androidbyexample.movies.screens

sealed interface Screen

data object MovieList: Screen
data class MovieDisplay(val id: String): Screen
