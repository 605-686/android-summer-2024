package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.movies.MovieViewModel
import kotlinx.coroutines.launch

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    val scope = rememberCoroutineScope()

    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            MovieDisplayUi(
                screen.id,
//                fetchMovie = { viewModel.getMovieWithCast(it) },
                fetchMovie = viewModel::getMovieWithCast,
            )
        }
        MovieList -> {
            val movies by viewModel.moviesFlow.collectAsStateWithLifecycle(initialValue = emptyList())

            MovieListUi(
                movies = movies,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                }
            )
        }
    }
}
