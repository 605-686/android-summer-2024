@file:OptIn(ExperimentalFoundationApi::class)

package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.repository.HasId

// NOTE:
//    THERE IS A PROBLEM HERE!!!
//    THERE IS A PROBLEM HERE!!!
//    THERE IS A PROBLEM HERE!!!
//      If all items would fit on the screen but some are covered by the bottom bar
//      dragging up briefly exposes the items by hiding the bottom bar, but then,
//      because everything fits, the scroll offset becomes zero again and the
//      bottom bar is re-exposed. The trouble is, we can't decide if the user is trying
//      to view the whole list, or would like access to the bottom nav!
//
//      Ooooooooof!
//
//      One workaround would be to remove the bottom bar. If that's ok for your app, great!
//      But how can we fix it and keep the bottom nav?
//      The contents could be small enough that the nav bar doesn't cover them; then everything is
//      fine. The contents could be large enough to go offscreen and require scrolling.
//      Again, that would be fine because scrolling to see them will keep that non-zero sizing.
//      We need to worry about that middle case: content is long enough to be hidden by part of
//      the bottom nav, but not longer than the screen space.
//
//      So what do we do?
//
//      In this case, the list thinks it needs to be scrollable because it's partially covered by
//      the bottom nav. When bottom nav is hidden, the list thinks it no longer needs to be
//      scrollable. So the trick is to ensure that the list thinks it needs to be scrollable
//      regardless of whether the bottom bar is visible or not.
//      The easiest way to do this would be to add a dummy item at the end of the list that's at
//      least as large as the bottom nav. Unfortunately we don't have that measurement available
//      to us. We can, however, look at the paddingValues passed in as well as the
//      size of the screen to tell use how much space is available for the list overall, and add
//      a dummy element of that size. I'll add that below with a ***SCROLL FIX*** comment
//
//      Not super desirable (as you can scroll all the items up off the screen);
//      I'll have to give it more thought and chat with some folks at work about it

@Composable
fun <T: HasId> List(
    state: LazyListState,
    items: List<T>,
    onItemClicked: (T) -> Unit,
    modifier: Modifier = Modifier,
    selectedIds: Set<String> = emptySet(),
    onSelectionToggle: (id: String) -> Unit = {},
    onClearSelections: () -> Unit = {},
    beforeList: (@Composable LazyItemScope.() -> Unit)? = null,
    itemContent: @Composable LazyItemScope.(T) -> Unit,
) {
    if (selectedIds.isNotEmpty()) {
        BackHandler {
            onClearSelections()
        }
    }

    // ***SCROLL FIX*** add a BoxWithConstraints around the list to give
    //    us access to BoxWithConstraintsScope.maxHeight. We'll use that far below
    BoxWithConstraints(modifier = modifier) {
        LazyColumn(state = state) {
            beforeList?.let {
                item {
                    beforeList()
                }
            }

            items(
                items = items,
                key = { it.id },
            ) { item ->
                val containerColor = if (item.id in selectedIds) {
                    MaterialTheme.colorScheme.secondary
                } else {
                    MaterialTheme.colorScheme.surface
                }

                val contentColor = MaterialTheme.colorScheme.contentColorFor(containerColor)

                Card(
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 8.dp,
                    ),
                    colors = CardDefaults.cardColors(
                        containerColor = containerColor,
                        contentColor = contentColor,
                    ),
                    modifier =
                    Modifier
                        .padding(8.dp)
                        .combinedClickable(
                            onClick = {
                                if (selectedIds.isEmpty()) {
                                    onItemClicked(item)
                                } else {
                                    onSelectionToggle(item.id)
                                }
                            },
                            onLongClick = {
                                onSelectionToggle(item.id)
                            },
                        )
                ) {
                    itemContent(item)
                }
            }
            item {
                // ***SCROLL FIX*** add a dummy box to the end of the list
                //    That's as big as the space we have available
                // Not ideal, but works for now. I'll try to find a better solution
                Box(modifier = Modifier.height(maxHeight))
            }
        }
    }
}