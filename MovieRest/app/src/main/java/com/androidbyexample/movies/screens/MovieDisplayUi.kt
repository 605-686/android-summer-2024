package com.androidbyexample.movies.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.components.Label
import com.androidbyexample.movies.repository.ActorDto
import com.androidbyexample.movies.repository.MovieWithCastDto

// NOTE - deleting roles doesn't currently work
@Composable
fun MovieDisplayUi(
    movieWithCast: MovieWithCastDto?,
    onActorClicked: (ActorDto) -> Unit,
    selectedIds: Set<String> = emptySet(),
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedItems: () -> Unit,
    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onEdit: (String) -> Unit,
) {
    ListScaffold(
        titleId = R.string.movie,
        onEdit = movieWithCast?.let {
            { // if we have a movie, pass this lambda as the edit function (else null from let)
                onEdit(it.movie.id)
            }
        },
        items = movieWithCast?.cast?.sortedBy { it.orderInCredits } ?: emptyList(),
        onItemClicked = { onActorClicked(it.actor) },
        selectedIds = selectedIds,
        onSelectionToggle = onSelectionToggle,
        onClearSelections = onClearSelections,
        onDeleteSelectedItems = onDeleteSelectedItems,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
        itemContent = { role ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(8.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Movie,
                    contentDescription = stringResource(id = R.string.movie),
                    modifier = Modifier.clickable {
                        onSelectionToggle(role.id)
                    }
                )
                Display(
                    text = stringResource(
                        R.string.cast_entry,
                        role.character,
                        role.actor.name,
                    )
                )
            }
        },
        beforeList = {
            Label(textId = R.string.title)
            Display(text = movieWithCast?.movie?.title ?: "")
            Label(textId = R.string.description)
            Display(text = movieWithCast?.movie?.description ?: "")
        },
    )
}
