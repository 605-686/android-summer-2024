@file:OptIn(ExperimentalFoundationApi::class)

package com.androidbyexample.movies.screens

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.repository.RatingDto

@Composable
fun RatingListUi(
    ratings: List<RatingDto>,
    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedRatings: () -> Unit,
    onResetDatabase: () -> Unit,
    onRatingClicked: (RatingDto) -> Unit,
    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
) {
    ListScaffold(
        titleId = R.string.ratings,
        items = ratings,
        onItemClicked = onRatingClicked,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        selectedIds = selectedIds,
        onClearSelections = onClearSelections,
        onSelectionToggle = onSelectionToggle,
        onDeleteSelectedItems = onDeleteSelectedRatings,
        onResetDatabase = onResetDatabase,
    ) {rating ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Star,
                contentDescription = stringResource(id = R.string.rating),
                modifier = Modifier.clickable {
                    onSelectionToggle(rating.id)
                },
            )
            Display(text = rating.name)
        }
    }
}
