package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.movies.MovieViewModel
import kotlinx.coroutines.launch

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    val scope = rememberCoroutineScope()

    val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())

    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            val movieWithCast by viewModel.getMovieWithCastFlow(screen.id).collectAsStateWithLifecycle(initialValue = null)

            MovieDisplayUi(
                movieWithCast,
                onActorClicked = { actor ->
                    viewModel.pushScreen(ActorDisplay(actor.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedItems = viewModel::deleteSelectedActors,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
                onEdit = { viewModel.pushScreen(MovieEdit(it)) },
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
            )
        }
        is MovieEdit -> {
            MovieEditUi(
                id = screen.id,
                fetchMovie = viewModel::getMovie,
                onMovieChange = viewModel::update
            )
        }
        is ActorDisplay -> {
            val actorWithFilmography by viewModel.getActorWithFilmographyFlow(screen.id).collectAsStateWithLifecycle(initialValue = null)

            ActorDisplayUi(
                actorWithFilmography,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedItems = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
            )
        }
        is RatingDisplay -> {
            val ratingWithMovies by viewModel.getRatingWithMoviesFlow(screen.id).collectAsStateWithLifecycle(initialValue = null)

            RatingDisplayUi(
                ratingWithMovies,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedItems = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
            )
        }
        MovieList -> {
            val movies by viewModel.moviesFlow.collectAsStateWithLifecycle(initialValue = emptyList())

            MovieListUi(
                movies = movies,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
// equivalent to
//                onClearSelections = { viewModel.clearSelectedIds() },
                onSelectionToggle = viewModel::toggleSelection,
// equivalent to
//                onSelectionToggle = { viewModel.toggleSelection(it) },
//                onSelectionToggle = { movie -> viewModel.toggleSelection(movie) },
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
            )
        }
        RatingList -> {
            val ratings by viewModel.ratingsFlow.collectAsStateWithLifecycle(initialValue = emptyList())

            RatingListUi(
                ratings = ratings,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onRatingClicked = { rating ->
                    viewModel.pushScreen(RatingDisplay(rating.id))
                },
                onDeleteSelectedRatings = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
            )
        }
        ActorList -> {
            val actors by viewModel.actorsFlow.collectAsStateWithLifecycle(initialValue = emptyList())

            ActorListUi(
                actors = actors,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onActorClicked = { actor ->
                    viewModel.pushScreen(ActorDisplay(actor.id))
                },
                onDeleteSelectedActors = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
            )
        }
    }
}
