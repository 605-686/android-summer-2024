plugins {
    alias(libs.plugins.jetbrains.kotlin.jvm)
    application
}

dependencies {
    implementation(libs.bundles.server)
}

application {
    mainClass.set("com.androidbyexample.movies.restserver.RunServerKt")
}
