package com.androidbyexample.movies.restserver

import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.Context
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.UriInfo


// NOTE: If Room were cross platform I would have directly used
//       our data module to store things in a database in this
//       server. Unfortunately, it's Android-only, so I'm
//       implementing an in-memory set of maps to track the
//       data. Kinda gross, but developing a real server isn't
//       important for the class. The important thing is how we
//       can communicate with the server
private fun <T> response(status: Response.Status, entity: T) =
    Response.status(status).entity(entity).build()

private fun <T> ok(entity: T) =
    response(Response.Status.OK, entity)

private fun <T> notFound(entity: T) =
    response(Response.Status.NOT_FOUND, entity)

private fun <T> created(entity: T) =
    response(Response.Status.CREATED, entity)


private val moviesByIdIndex = mutableMapOf<String, Movie>()
private val actorsByIdIndex = mutableMapOf<String, Actor>()
private val ratingsByIdIndex = mutableMapOf<String, Rating>()
private val rolesByMovieIdIndex = mutableMapOf<String, MutableList<Role>>()
private val rolesByActorIdIndex = mutableMapOf<String, MutableList<Role>>()
private val moviesByRatingIdIndex = mutableMapOf<String, MutableList<Movie>>()

private val notFoundRating = Rating("-", "NOT FOUND", "NOT FOUND")
private val notFoundMovie = Movie("-", "NOT FOUND", "NOT FOUND", "--")
private val notFoundMovieWithRoles = MovieWithCast(notFoundMovie, emptyList())
private val notFoundActor = Actor("-", "NOT FOUND")
private val notFoundActorWithRoles = ActorWithFilmography(notFoundActor, emptyList())
private val notFoundRatingWithMovies = RatingWithMovies(notFoundRating, emptyList())

@Path("/")
class RestController {
    @GET
    @Path("rating")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRatings(): Response = ok(ratingsByIdIndex.values.sortedBy { it.id })

    @GET
    @Path("movie")
    @Produces(MediaType.APPLICATION_JSON)
    fun getMovies(): Response = ok(moviesByIdIndex.values.sortedBy { it.title })

    @GET
    @Path("actor")
    @Produces(MediaType.APPLICATION_JSON)
    fun getActors(): Response = ok(actorsByIdIndex.values.sortedBy { it.name })

    @GET
    @Path("rating/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRating(@PathParam("id") id: String): Rating =
        ratingsByIdIndex[id] ?: notFoundRating

    @GET
    @Path("movie/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getMovie(@PathParam("id") id: String): Movie =
        moviesByIdIndex[id] ?: notFoundMovie

    @GET
    @Path("actor/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getActor(@PathParam("id") id: String): Actor =
        actorsByIdIndex[id] ?: notFoundActor

    @GET
    @Path("rating/{id}/movies")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRatingWithMovies(@PathParam("id") id: String): RatingWithMovies =
        ratingsByIdIndex[id]?.let { rating ->
            val movies = moviesByRatingIdIndex[id] ?: emptyList()
            RatingWithMovies(rating, movies)
        } ?: notFoundRatingWithMovies

    @GET
    @Path("movie/{id}/cast")
    @Produces(MediaType.APPLICATION_JSON)
    fun getMovieWithRoles(@PathParam("id") id: String): MovieWithCast =
        moviesByIdIndex[id]?.let { movie ->
            val roles =
                rolesByMovieIdIndex[id]
                    ?.map { role ->
                        RoleWithActor(
                            actor = actorsByIdIndex[role.actorId] ?: throw IllegalStateException(),
                            character = role.character,
                            orderInCredits = role.orderInCredits,
                        )
                    }
                    ?: emptyList()

            MovieWithCast(movie, roles)
        } ?: notFoundMovieWithRoles

    @GET
    @Path("actor/{id}/filmography")
    @Produces(MediaType.APPLICATION_JSON)
    fun getActorWithRoles(@PathParam("id") id: String): ActorWithFilmography =
        actorsByIdIndex[id]?.let { actor ->
            val roles =
                rolesByActorIdIndex[id]
                    ?.map { role ->
                        RoleWithMovie(
                            movie = moviesByIdIndex[role.movieId] ?: throw IllegalStateException(),
                            character = role.character,
                            orderInCredits = role.orderInCredits,
                        )
                    }
                    ?: emptyList()
            ActorWithFilmography(actor, roles)
        } ?: notFoundActorWithRoles

    @PUT
    @Path("movie/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    fun updateMovie(@PathParam("id") id: String, movie: Movie): Response {
        moviesByIdIndex[id] = movie
        moviesByRatingIdIndex.getOrPut(movie.ratingId) { mutableListOf() }.add(movie)
        return ok(1)
    }

    @PUT
    @Path("actor/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    fun updateActor(@PathParam("id") id: String, actor: Actor): Response {
        actorsByIdIndex[id] = actor
        return ok(1)
    }

    @PUT
    @Path("rating/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    fun updateRating(@PathParam("id") id: String, rating: Rating): Response {
        ratingsByIdIndex[id] = rating
        return ok(1)
    }

    private operator fun MutableMap<String, MutableMap<String, Role>>.set(id1: String, id2: String, role: Role) {
        val roles = this[id1] ?: mutableMapOf<String, Role>().apply { this@set[id1] = this }
        roles[id2] = role
    }

    private operator fun MutableMap<String, MutableMap<String, Role>>.get(id1: String, id2: String) =
        this[id1]?.get(id2)

    @POST
    @Path("rating/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun createRating(@Context uriInfo: UriInfo, rating: Rating): Response {
        ratingsByIdIndex[rating.id] = rating
        return created(rating)
    }

    @POST
    @Path("movie/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun createMovie(@Context uriInfo: UriInfo, movie: Movie): Response {
        moviesByIdIndex[movie.id] = movie
        return created(movie)
    }

    @POST
    @Path("actor/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun createActor(@Context uriInfo: UriInfo, actor: Actor): Response {
        actorsByIdIndex[actor.id] = actor
        return created(actor)
    }

    @DELETE
    @Path("movie/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun deleteMovie(@PathParam("id") id: String): Response =
        if (moviesByIdIndex[id] == null) {
            notFound(0)
        } else {
            moviesByIdIndex.remove(id)
            rolesByMovieIdIndex.remove(id) // remove all roles
            rolesByActorIdIndex.values.forEach { roles ->
                roles.removeIf { it.movieId == id }
            }
            ok(1)
        }

    @DELETE
    @Path("actor/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun deleteActor(@PathParam("id") id: String): Response =
        if (actorsByIdIndex[id] == null) {
            notFound(0)
        } else {
            actorsByIdIndex.remove(id)
            rolesByActorIdIndex.remove(id) // remove all roles
            rolesByMovieIdIndex.values.forEach { roles -> // remove filmography for those movies
                roles.removeIf { it.actorId == id }
            }
            ok(1)
        }

    @DELETE
    @Path("rating/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    fun deleteRating(@PathParam("id") id: String): Response =
        if (ratingsByIdIndex[id] == null) {
            notFound(0)
        } else {
            ratingsByIdIndex.remove(id)
            // delete associated movies
            moviesByRatingIdIndex[id]?.forEach {
                moviesByIdIndex.remove(it.id)
            }
            moviesByRatingIdIndex.remove(id)
            ok(1)
        }

    private fun insertMovies(vararg newMovies: Movie) {
        newMovies.forEach { movie ->
            moviesByIdIndex[movie.id] = movie
            moviesByRatingIdIndex.getOrCreate(movie.ratingId).add(movie)
        }
    }

    private fun insertActors(vararg newActors: Actor) {
        newActors.forEach { actor ->
            actorsByIdIndex[actor.id] = actor
        }
    }
    private fun insertRatings(vararg newRatings: Rating) {
        newRatings.forEach { rating ->
            ratingsByIdIndex[rating.id] = rating
        }
    }

    private fun insertRoles(vararg newRoles: Role) {
        newRoles.forEach { role ->
            rolesByActorIdIndex.getOrCreate(role.actorId).add(role)
            rolesByMovieIdIndex.getOrCreate(role.movieId).add(role)
        }
    }
    private fun <K, V> MutableMap<K, MutableList<V>>.getOrCreate(key: K) =
        this[key] ?: mutableListOf<V>().apply {
            this@getOrCreate[key] = this
        }

    @GET
    @Path("reset")
    @Produces(MediaType.TEXT_PLAIN)
    fun resetDatabase(): Response {
        ratingsByIdIndex.clear()
        moviesByIdIndex.clear()
        actorsByIdIndex.clear()
        rolesByActorIdIndex.clear()
        rolesByMovieIdIndex.clear()

        insertRatings(
            Rating(id = "r0", name = "Not Rated", description = "Not yet rated"),
            Rating(id = "r1", name = "G", description = "General Audiences"),
            Rating(id = "r2", name = "PG", description = "Parental Guidance Suggested"),
            Rating(id = "r3", name = "PG-13", description = "Unsuitable for those under 13"),
            Rating(id = "r4", name = "R", description = "Restricted - 17 and older"),
        )

        insertMovies(
            Movie("m1", "The Transporter", "Jason Statham kicks a guy in the face", "r3"),
            Movie("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face", "r4"),
            Movie("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff", "r3"),
            Movie("m4", "Jumanji", "The Rock smolders", "r3"),
        )

        insertActors(
            Actor("a1", "Jason Statham"),
            Actor("a2", "The Rock"),
            Actor("a3", "Shu Qi"),
            Actor("a4", "Amber Valletta"),
            Actor("a5", "Kevin Hart"),
        )
        insertRoles(
            Role("m1", "a1", "Frank Martin", 1),
            Role("m1", "a3", "Lai", 2),
            Role("m2", "a1", "Frank Martin", 1),
            Role("m2", "a4", "Audrey Billings", 2),
            Role("m3", "a2", "Hobbs", 1),
            Role("m3", "a1", "Shaw", 2),
            Role("m4", "a2", "Spencer", 1),
            Role("m4", "a5", "Fridge", 2),
        )
        return ok(1)
    }
}
