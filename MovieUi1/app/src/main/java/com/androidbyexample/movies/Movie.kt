package com.androidbyexample.movies

data class Movie(
    val title: String,
    val description: String,
)
