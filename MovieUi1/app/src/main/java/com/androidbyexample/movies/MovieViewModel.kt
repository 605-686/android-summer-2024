package com.androidbyexample.movies

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.androidbyexample.movies.screens.MovieList
import com.androidbyexample.movies.screens.Screen

class MovieViewModel: ViewModel() {
    private var screenStack = listOf<Screen>(MovieList)
        set(value) {
            field = value
            currentScreen = value.lastOrNull()
        }

    var currentScreen by mutableStateOf<Screen?>(MovieList)
        private set

    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        screenStack = screenStack.dropLast(1)
    }

    val movies: List<Movie> = listOf(
        Movie("The Transporter", "Jason Statham kicks a guy in the face"),
        Movie("Transporter 2", "Jason Statham kicks a bunch of guys in the face"),
        Movie("Hobbs and Shaw", "Cars, Explosions and Stuff"),
        Movie("Jumanji - Welcome to the Jungle", "The Rock smolders"),
    )
}
