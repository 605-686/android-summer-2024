package com.androidbyexample.movies.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@Composable
fun Label(
    @StringRes textId: Int,
) {
    Text(
        text = stringResource(id = textId),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
    )
}
