package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.androidbyexample.movies.Movie
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.components.Label

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieDisplayUi(
    movie: Movie,
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = movie.title)
                }
            )
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .verticalScroll(rememberScrollState())
        ) {
            Label(textId = R.string.title)
            Display(text = movie.title)
            Label(textId = R.string.description)
            Display(text = movie.description)
        }
    }
}
