package com.androidbyexample.movies.screens

import com.androidbyexample.movies.Movie

sealed interface Screen

object MovieList: Screen
data class MovieDisplay(val movie: Movie): Screen
