package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import com.androidbyexample.movies.MovieViewModel

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            MovieDisplayUi(screen.movie)
        }
        MovieList -> {
            MovieListUi(viewModel.movies) { movie ->
                viewModel.pushScreen(MovieDisplay(movie))
            }
        }
    }
}
