package com.androidbyexample.movies

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import com.androidbyexample.movies.screens.Ui
import com.androidbyexample.movies.ui.theme.MovieUi2Theme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel> { MovieViewModel.Factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            MovieUi2Theme {
                Ui(viewModel) {
                    finish()
                }
            }
        }
    }
}
