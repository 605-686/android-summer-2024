package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.components.Label
import com.androidbyexample.movies.repository.ActorWithFilmographyDto
import com.androidbyexample.movies.repository.MovieDto
import com.androidbyexample.movies.repository.MovieWithCastDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ActorDisplayUi(
    id: String,
    fetchActor: suspend (String) -> ActorWithFilmographyDto,
    onMovieClicked: (MovieDto) -> Unit,
) {
    var actorWithFilmography by remember { mutableStateOf<ActorWithFilmographyDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            actorWithFilmography = fetchActor(id)
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = actorWithFilmography?.actor?.name ?: "(fetching)")
                }
            )
        }
    ) { paddingValues ->
         List(
            items = actorWithFilmography?.filmography?.sortedBy { it.orderInCredits } ?: emptyList(),
            onItemClicked = { onMovieClicked(it.movie) },
             beforeList = {
                 Label(textId = R.string.name)
                 Display(text = actorWithFilmography?.actor?.name ?: "")
                 Label(textId = R.string.movies_starring, actorWithFilmography?.actor?.name ?: "")
             },
            modifier = Modifier.padding(paddingValues)
        ) { role ->
            Display(text = role.movie.title)
        }
    }
}
