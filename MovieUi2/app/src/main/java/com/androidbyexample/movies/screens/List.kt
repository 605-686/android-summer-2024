@file:OptIn(ExperimentalFoundationApi::class)

package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.repository.HasId

@Composable
fun <T: HasId> List(
    items: List<T>,
    onItemClicked: (T) -> Unit,
    modifier: Modifier = Modifier,
    selectedIds: Set<String> = emptySet(),
    onSelectionToggle: (id: String) -> Unit = {},
    onClearSelections: () -> Unit = {},
    beforeList: @Composable LazyItemScope.() -> Unit = {},
    itemContent: @Composable LazyItemScope.(T) -> Unit,
) {
    if (selectedIds.isNotEmpty()) {
        BackHandler {
            onClearSelections()
        }
    }

    LazyColumn(modifier = modifier) {
        item {
            beforeList()
        }

        items(
            items = items,
            key = { it.id },
        ) { item ->
            val containerColor = if (item.id in selectedIds) {
                MaterialTheme.colorScheme.secondary
            } else {
                MaterialTheme.colorScheme.surface
            }

            val contentColor = MaterialTheme.colorScheme.contentColorFor(containerColor)

            Card(
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp,
                ),
                colors = CardDefaults.cardColors(
                    containerColor = containerColor,
                    contentColor = contentColor,
                ),
                modifier =
                    Modifier
                        .padding(8.dp)
                        .combinedClickable(
                            onClick = {
                                if (selectedIds.isEmpty()) {
                                    onItemClicked(item)
                                } else {
                                    onSelectionToggle(item.id)
                                }
                            },
                            onLongClick = {
                                onSelectionToggle(item.id)
                            },
                        )
            ) {
                itemContent(item)
            }
        }
    }
}