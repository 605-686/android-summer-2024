package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.components.Label
import com.androidbyexample.movies.repository.ActorDto
import com.androidbyexample.movies.repository.MovieWithCastDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieDisplayUi(
    id: String,
    fetchMovie: suspend (String) -> MovieWithCastDto,
    onActorClicked: (ActorDto) -> Unit,
) {
    var movieWithCast by remember { mutableStateOf<MovieWithCastDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            movieWithCast = fetchMovie(id)
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = movieWithCast?.movie?.title ?: "(fetching)")
                }
            )
        }
    ) { paddingValues ->
         List(
            items = movieWithCast?.cast?.sortedBy { it.orderInCredits } ?: emptyList(),
            onItemClicked = { onActorClicked(it.actor) },
             beforeList = {
                 Label(textId = R.string.title)
                 Display(text = movieWithCast?.movie?.title ?: "")
                 Label(textId = R.string.description)
                 Display(text = movieWithCast?.movie?.description ?: "")
             },
            modifier = Modifier.padding(paddingValues),
        ) { role ->
            Display(
                text = stringResource(
                    id = R.string.cast_entry,
                    role.character,
                    role.actor.name,
                )
            )
        }
    }
}
