package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.components.Label
import com.androidbyexample.movies.repository.MovieDto
import com.androidbyexample.movies.repository.RatingWithMoviesDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RatingDisplayUi(
    id: String,
    fetchRating: suspend (String) -> RatingWithMoviesDto,
    onMovieClicked: (MovieDto) -> Unit,
) {
    var ratingWithMovies by remember { mutableStateOf<RatingWithMoviesDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            ratingWithMovies = fetchRating(id)
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = ratingWithMovies?.rating?.name ?: "(fetching)")
                }
            )
        }
    ) { paddingValues ->
         List(
            items = ratingWithMovies?.movies?.sortedBy { it.title } ?: emptyList(),
            onItemClicked = { onMovieClicked(it) },
             beforeList = {
                 Label(textId = R.string.name)
                 Display(text = ratingWithMovies?.rating?.name ?: "")
                 Label(textId = R.string.description)
                 Display(text = ratingWithMovies?.rating?.description ?: "")
             },
            modifier = Modifier.padding(paddingValues),
        ) { movie ->
            Display(
                text = movie.title
            )
        }
    }
}
