package com.androidbyexample.movies.screens

sealed interface Screen

data object MovieList: Screen
data object RatingList: Screen
data object ActorList: Screen
data class MovieDisplay(val id: String): Screen
data class RatingDisplay(val id: String): Screen
data class ActorDisplay(val id: String): Screen
