package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.movies.MovieViewModel
import kotlinx.coroutines.launch

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    val scope = rememberCoroutineScope()

    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            MovieDisplayUi(
                screen.id,
//                fetchMovie = { viewModel.getMovieWithCast(it) },
                fetchMovie = viewModel::getMovieWithCast,
                onActorClicked = { actor ->
                    viewModel.pushScreen(ActorDisplay(actor.id))
                },
            )
        }
        is ActorDisplay -> {
            ActorDisplayUi(
                screen.id,
                fetchActor = viewModel::getActorWithFilmography,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
            )
        }
        is RatingDisplay -> {
            RatingDisplayUi(
                screen.id,
                fetchRating = viewModel::getRatingWithMovies,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(ActorDisplay(movie.id))
                },
            )
        }
        MovieList -> {
            val movies by viewModel.moviesFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())

            MovieListUi(
                movies = movies,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
// equivalent to
//                onClearSelections = { viewModel.clearSelectedIds() },
                onSelectionToggle = viewModel::toggleSelection,
// equivalent to
//                onSelectionToggle = { viewModel.toggleSelection(it) },
//                onSelectionToggle = { movie -> viewModel.toggleSelection(movie) },
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
            )
        }
        RatingList -> {
            val ratings by viewModel.ratingsFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())

            RatingListUi(
                ratings = ratings,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onRatingClicked = { rating ->
                    viewModel.pushScreen(RatingDisplay(rating.id))
                },
                onDeleteSelectedRatings = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
            )
        }
        ActorList -> {
            val actors by viewModel.actorsFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())

            ActorListUi(
                actors = actors,
                onResetDatabase = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onActorClicked = { actor ->
                    viewModel.pushScreen(ActorDisplay(actor.id))
                },
                onDeleteSelectedActors = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::pushScreen,
            )
        }
    }
}
