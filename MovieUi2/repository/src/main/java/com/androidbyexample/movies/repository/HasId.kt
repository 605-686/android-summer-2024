package com.androidbyexample.movies.repository

interface HasId {
    val id: String
}