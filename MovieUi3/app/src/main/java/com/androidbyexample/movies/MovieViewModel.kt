package com.androidbyexample.movies

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.androidbyexample.movies.repository.ActorDto
import com.androidbyexample.movies.repository.MovieDatabaseRepository
import com.androidbyexample.movies.repository.MovieDto
import com.androidbyexample.movies.repository.MovieRepository
import com.androidbyexample.movies.repository.RatingDto
import com.androidbyexample.movies.screens.MovieList
import com.androidbyexample.movies.screens.Screen
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch

@OptIn(FlowPreview::class)
class MovieViewModel(
    private val repository: MovieRepository,
): ViewModel(), MovieRepository by repository {

    private val _selectedIdsFlow = MutableStateFlow<Set<String>>(emptySet())
    val selectedIdsFlow: Flow<Set<String>> = _selectedIdsFlow

    private var screenStack = listOf<Screen>(MovieList)
        set(value) {
            field = value
            clearSelectedIds()
            currentScreen = value.lastOrNull()
        }

    var currentScreen by mutableStateOf<Screen?>(MovieList)
        private set

    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        screenStack = screenStack.dropLast(1)
    }

    fun update(movie: MovieDto) {
        movieUpdateFlow.value = movie
    }
    fun update(actor: ActorDto) {
        actorUpdateFlow.value = actor
    }
    fun update(rating: RatingDto) {
        ratingUpdateFlow.value = rating
    }
    // using a debounced flow as a person-update queue
    private val movieUpdateFlow = MutableStateFlow<MovieDto?>(null)
    private val actorUpdateFlow = MutableStateFlow<ActorDto?>(null)
    private val ratingUpdateFlow = MutableStateFlow<RatingDto?>(null)
    init {
        viewModelScope.launch {
            movieUpdateFlow.debounce(500).collect { movie ->
                movie?.let { repository.upsert(it) }
            }
        }
        viewModelScope.launch {
            actorUpdateFlow.debounce(500).collect { actor ->
                actor?.let { repository.upsert(it) }
            }
        }
        viewModelScope.launch {
            ratingUpdateFlow.debounce(500).collect { rating ->
                rating?.let { repository.upsert(it) }
            }
        }
    }

    fun clearSelectedIds() {
        _selectedIdsFlow.value = emptySet()
    }
    fun toggleSelection(id: String) {
        if (id in _selectedIdsFlow.value) {
            _selectedIdsFlow.value -= id
        } else {
            _selectedIdsFlow.value += id
        }
    }

    fun deleteSelectedMovies() {
        viewModelScope.launch {
            deleteMoviesById(_selectedIdsFlow.value)
            clearSelectedIds()
        }
    }
    fun deleteSelectedActors() {
        viewModelScope.launch {
            deleteActorsById(_selectedIdsFlow.value)
            clearSelectedIds()
        }
    }
    fun deleteSelectedRatings() {
        viewModelScope.launch {
            deleteRatingsById(_selectedIdsFlow.value)
            clearSelectedIds()
        }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(
                    MovieDatabaseRepository.create(application)
                ) as T
            }
        }
    }
}
