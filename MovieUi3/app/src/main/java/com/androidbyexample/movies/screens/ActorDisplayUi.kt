package com.androidbyexample.movies.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.components.Label
import com.androidbyexample.movies.repository.ActorWithFilmographyDto
import com.androidbyexample.movies.repository.MovieDto

// NOTE - deleting roles doesn't currently work
@Composable
fun ActorDisplayUi(
    actorWithFilmography: ActorWithFilmographyDto?,
    onMovieClicked: (MovieDto) -> Unit,
    selectedIds: Set<String> = emptySet(),
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedItems: () -> Unit,
    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
) {
    ListScaffold(
        titleId = R.string.actor,
        items = actorWithFilmography?.filmography?.sortedBy { it.orderInCredits } ?: emptyList(),
        onItemClicked = { onMovieClicked(it.movie) },
        selectedIds = selectedIds,
        onSelectionToggle = onSelectionToggle,
        onClearSelections = onClearSelections,
        onDeleteSelectedItems = onDeleteSelectedItems,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
        itemContent = { role ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(8.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Movie,
                    contentDescription = stringResource(id = R.string.movie),
                    modifier = Modifier.clickable {
                        onSelectionToggle(role.id)
                    }
                )
                Display(text = role.movie.title)
            }
        },
        beforeList = {
            Label(textId = R.string.name)
            Display(text = actorWithFilmography?.actor?.name ?: "")
            Label(textId = R.string.movies_starring, actorWithFilmography?.actor?.name ?: "")
        },
    )
}
