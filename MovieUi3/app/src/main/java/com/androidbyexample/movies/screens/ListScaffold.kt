@file:OptIn(ExperimentalMaterial3Api::class)

package com.androidbyexample.movies.screens

import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.ScreenSelectButton
import com.androidbyexample.movies.repository.HasId

@Composable
fun <T: HasId> ListScaffold(
    @StringRes titleId: Int,
    items: List<T>,
    onItemClicked: (T) -> Unit,
    modifier: Modifier = Modifier,
    selectedIds: Set<String> = emptySet(),
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedItems: () -> Unit,
    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onEdit: (() -> Unit)? = null,
    beforeList: (@Composable LazyItemScope.() -> Unit)? = null,
    itemContent: @Composable LazyItemScope.(T) -> Unit,
) {
    val lazyListState = remember { LazyListState() }
    val showBottomBar by remember(selectedIds) {
        derivedStateOf {
            lazyListState.firstVisibleItemScrollOffset == 0 && selectedIds.isEmpty()
        }
    }

    Scaffold(
        modifier = modifier,
        topBar = {
            if (selectedIds.isEmpty()) {
                TopAppBar(
                    title = { Text(text = stringResource(titleId)) },
                    actions = {
                        onEdit?.let {
                            IconButton(onClick = onEdit) {
                                Icon(
                                    imageVector = Icons.Default.Edit,
                                    contentDescription = stringResource(R.string.edit)
                                )
                            }
                        }
                        IconButton(onClick = onResetDatabase) {
                            Icon(
                                imageVector = Icons.Default.Refresh,
                                contentDescription = stringResource(id = R.string.reset_database)
                            )
                        }
                    }
                )
            } else {
                TopAppBar(
                    navigationIcon = {
                        Icon(
                            imageVector = Icons.AutoMirrored.Default.ArrowBack,
                            contentDescription = stringResource(id = R.string.clear_selections),
                            modifier = Modifier.clickable(onClick = onClearSelections),
                        )
                    },
                    title = {
                        Text(
                            text = selectedIds.size.toString(),
                            modifier = Modifier.padding(8.dp)
                        )
                    },
                    actions = {
                        IconButton(onClick = onDeleteSelectedItems) {
                            Icon(
                                imageVector = Icons.Default.Delete,
                                contentDescription = stringResource(id = R.string.delete_selected_items)
                            )
                        }
                    }
                )
            }
        },
        bottomBar = {
            AnimatedVisibility(
                visible = showBottomBar,
                enter = expandVertically() + fadeIn(),
                exit = shrinkVertically() + fadeOut(),
            ) {
                NavigationBar {
                    ScreenSelectButton(
                        currentScreen = currentScreen,
                        targetScreen = RatingList,
                        imageVector = Icons.Default.Star,
                        labelId = R.string.ratings,
                        onSelectListScreen = onSelectListScreen,
                    )
                    ScreenSelectButton(
                        currentScreen = currentScreen,
                        targetScreen = MovieList,
                        imageVector = Icons.Default.Star,
                        labelId = R.string.movies,
                        onSelectListScreen = onSelectListScreen,
                    )
                    ScreenSelectButton(
                        currentScreen = currentScreen,
                        targetScreen = ActorList,
                        imageVector = Icons.Default.Person,
                        labelId = R.string.actors,
                        onSelectListScreen = onSelectListScreen,
                    )
                }
            }
        },
    ) { paddingValues ->
        List(
            state = lazyListState,
            items = items,
            onItemClicked = onItemClicked,
            selectedIds = selectedIds,
            onSelectionToggle = onSelectionToggle,
            onClearSelections = onClearSelections,
            itemContent = itemContent,
            beforeList = beforeList,
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues),
        )
    }
}