//@file:OptIn(ExperimentalFoundationApi::class)
//
//package com.androidbyexample.movies.screens
//
//import androidx.activity.compose.BackHandler
//import androidx.compose.foundation.ExperimentalFoundationApi
//import androidx.compose.foundation.clickable
//import androidx.compose.foundation.combinedClickable
//import androidx.compose.foundation.layout.Column
//import androidx.compose.foundation.layout.Row
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.foundation.layout.padding
//import androidx.compose.foundation.lazy.LazyColumn
//import androidx.compose.foundation.lazy.items
//import androidx.compose.foundation.rememberScrollState
//import androidx.compose.foundation.verticalScroll
//import androidx.compose.material.icons.Icons
//import androidx.compose.material.icons.automirrored.filled.ArrowBack
//import androidx.compose.material.icons.filled.ArrowBack
//import androidx.compose.material.icons.filled.Delete
//import androidx.compose.material.icons.filled.Refresh
//import androidx.compose.material.icons.filled.Star
//import androidx.compose.material3.Card
//import androidx.compose.material3.CardDefaults
//import androidx.compose.material3.ExperimentalMaterial3Api
//import androidx.compose.material3.Icon
//import androidx.compose.material3.IconButton
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.material3.Scaffold
//import androidx.compose.material3.Text
//import androidx.compose.material3.TopAppBar
//import androidx.compose.material3.contentColorFor
//import androidx.compose.runtime.Composable
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.res.stringResource
//import androidx.compose.ui.unit.dp
//import com.androidbyexample.movies.R
//import com.androidbyexample.movies.components.Display
//import com.androidbyexample.movies.repository.HasId
//import com.androidbyexample.movies.repository.MovieDto
//
//@OptIn(ExperimentalMaterial3Api::class)
//@Composable
//fun MovieListUi(
//    movies: List<MovieDto>,
//    selectedIds: Set<String>,
//    onSelectionToggle: (id: String) -> Unit,
//    onClearSelections: () -> Unit,
//    onDeleteSelectedMovies: () -> Unit,
//    onResetDatabase: () -> Unit,
//    onMovieClicked: (MovieDto) -> Unit,
//) {
//    if (selectedIds.isNotEmpty()) {
//        BackHandler {
//            onClearSelections()
//        }
//    }
//
//    Scaffold(
//        topBar = {
//            if (selectedIds.isEmpty()) {
//                TopAppBar(
//                    title = { Text(text = stringResource(R.string.movies)) },
//                    actions = {
//                        IconButton(onClick = onResetDatabase) {
//                            Icon(
//                                imageVector = Icons.Default.Refresh,
//                                contentDescription = stringResource(id = R.string.reset_database)
//                            )
//                        }
//                    }
//                )
//            } else {
//                TopAppBar(
//                    navigationIcon = {
//                        Icon(
//                            imageVector = Icons.AutoMirrored.Default.ArrowBack,
//                            contentDescription = stringResource(id = R.string.clear_selections),
//                            modifier = Modifier.clickable(onClick = onClearSelections),
//                        )
//                    },
//                    title = {
//                        Text(
//                            text = selectedIds.size.toString(),
//                            modifier = Modifier.padding(8.dp)
//                        )
//                    },
//                    actions = {
//                        IconButton(onClick = onDeleteSelectedMovies) {
//                            Icon(
//                                imageVector = Icons.Default.Delete,
//                                contentDescription = stringResource(id = R.string.delete_selected_items)
//                            )
//                        }
//                    }
//                )
//            }
//        },
//        modifier = Modifier.fillMaxSize()
//    ) { paddingValues ->
//        LazyColumn(
//            modifier = Modifier
//                .padding(paddingValues)
//                .fillMaxSize()
//        ) {
//            item {
//                Display(text = "I'm in a dynamic list!")
//            }
//            items(
//                items = movies,
//                key = { it.id },
//            ) { movie ->
//
//                val containerColor = if (movie.id in selectedIds) {
//                    MaterialTheme.colorScheme.secondary
//                } else {
//                    MaterialTheme.colorScheme.surface
//                }
//
//                val contentColor = MaterialTheme.colorScheme.contentColorFor(containerColor)
//
//                Card(
//                    elevation = CardDefaults.cardElevation(
//                        defaultElevation = 8.dp,
//                    ),
//                    colors = CardDefaults.cardColors(
//                        containerColor = containerColor,
//                        contentColor = contentColor,
//                    ),
//                    modifier =
//                    Modifier
//                        .padding(8.dp)
//                        .combinedClickable(
//                            onClick = {
//                                if (selectedIds.isEmpty()) {
//                                    onMovieClicked(movie)
//                                } else {
//                                    onSelectionToggle(movie.id)
//                                }
//                            },
//                            onLongClick = {
//                                onSelectionToggle(movie.id)
//                            },
//                        )
//                ) {
//                    Row(
//                        verticalAlignment = Alignment.CenterVertically,
//                        modifier = Modifier.padding(8.dp)
//                    ) {
//                        Icon(
//                            imageVector = Icons.Default.Star,
//                            contentDescription = stringResource(id = R.string.movie),
//                            modifier = Modifier.clickable {
//                                onSelectionToggle(movie.id)
//                            },
//                        )
//                        Display(text = movie.title)
//                    }
//                }
//            }
//            item {
//                Display(text = "End of my dynamic list!")
//            }
//        }
//    }
//}
