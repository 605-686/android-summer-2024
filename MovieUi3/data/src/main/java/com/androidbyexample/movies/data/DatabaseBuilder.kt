package com.androidbyexample.movies.data

import android.content.Context
import androidx.room.Room

fun createDao(context: Context) =
    Room.databaseBuilder(
        context,
        MovieDatabase::class.java,
        "MOVIES"
    )
        .build()
        .dao
