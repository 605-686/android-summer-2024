package com.androidbyexample.movies.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.UUID

@Entity(
    indices = [
        Index("ratingId")
    ],
    foreignKeys = [
        ForeignKey(
            entity = RatingEntity::class,
            parentColumns = ["id"],
            childColumns = ["ratingId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        )
    ]
)
class MovieEntity(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var title: String,
    val description: String,
    val ratingId: String,
)

data class RoleWithActor(
    @Embedded
    val role: RoleEntity,
    @Relation(
        parentColumn = "actorId",
        entityColumn = "id",
    )
    val actor: ActorEntity
)
data class MovieWithCast(
    @Embedded
    val movie: MovieEntity,
    @Relation(
        entity = RoleEntity::class,
        parentColumn = "id",
        entityColumn = "movieId",
    )
    val rolesWithActors: List<RoleWithActor>
)