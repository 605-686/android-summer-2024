package com.androidbyexample.movies.repository

import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<RatingDto>>
    val moviesFlow: Flow<List<MovieDto>>
    val actorsFlow: Flow<List<ActorDto>>
    suspend fun getRatingWithMovies(id: String): RatingWithMoviesDto
    fun getRatingWithMoviesFlow(id: String): Flow<RatingWithMoviesDto>
    suspend fun getMovieWithCast(id: String): MovieWithCastDto
    fun getMovieWithCastFlow(id: String): Flow<MovieWithCastDto>
    suspend fun getActorWithFilmography(id: String): ActorWithFilmographyDto
    fun getActorWithFilmographyFlow(id: String): Flow<ActorWithFilmographyDto>

    suspend fun getMovie(id: String): MovieDto

    suspend fun insert(movie: MovieDto)
    suspend fun insert(actor: ActorDto)
    suspend fun insert(rating: RatingDto)

    suspend fun upsert(movie: MovieDto)
    suspend fun upsert(actor: ActorDto)
    suspend fun upsert(rating: RatingDto)

    suspend fun resetDatabase()

    suspend fun deleteMoviesById(ids: Set<String>)
    suspend fun deleteActorsById(ids: Set<String>)
    suspend fun deleteRatingsById(ids: Set<String>)
}