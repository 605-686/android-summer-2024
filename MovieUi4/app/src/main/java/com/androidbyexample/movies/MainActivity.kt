package com.androidbyexample.movies

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import com.androidbyexample.movies.screens.MovieDisplay
import com.androidbyexample.movies.screens.MovieList
import com.androidbyexample.movies.screens.Ui
import com.androidbyexample.movies.ui.theme.MovieUi4Theme

const val MOVIE_ID_EXTRA = "movieId"

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel> { MovieViewModel.Factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleMovieId(intent)
        enableEdgeToEdge()
        setContent {
            MovieUi4Theme {
                Ui(viewModel) {
                    finish()
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleMovieId(intent)
    }

    private fun handleMovieId(intent: Intent) {
//        val movieId = intent.extras?.getString(MOVIE_ID_EXTRA)
//        if (movieId != null) {
//            viewModel.setScreens(MovieList, MovieDisplay(movieId))
//        }

        // same function as above
        intent
            .extras
            ?.getString(MOVIE_ID_EXTRA)
            ?.let { movieId ->
                viewModel.setScreens(MovieList, MovieDisplay(movieId))
            }
    }
}
