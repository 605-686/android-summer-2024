package com.androidbyexample.movies.glance

import android.content.Context
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.datastore.core.DataStore
import androidx.glance.GlanceId
import androidx.glance.GlanceModifier
import androidx.glance.GlanceTheme
import androidx.glance.LocalContext
import androidx.glance.action.ActionParameters
import androidx.glance.action.actionParametersOf
import androidx.glance.action.actionStartActivity
import androidx.glance.action.clickable
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.appWidgetBackground
import androidx.glance.appwidget.lazy.LazyColumn
import androidx.glance.appwidget.lazy.items
import androidx.glance.appwidget.provideContent
import androidx.glance.background
import androidx.glance.currentState
import androidx.glance.layout.Box
import androidx.glance.layout.Column
import androidx.glance.layout.fillMaxSize
import androidx.glance.layout.fillMaxWidth
import androidx.glance.layout.padding
import androidx.glance.state.GlanceStateDefinition
import androidx.glance.text.FontWeight
import androidx.glance.text.Text
import androidx.glance.text.TextStyle
import com.androidbyexample.movies.MOVIE_ID_EXTRA
import com.androidbyexample.movies.MainActivity
import com.androidbyexample.movies.R
import com.androidbyexample.movies.repository.MovieDatabaseRepository
import com.androidbyexample.movies.repository.MovieDto
import kotlinx.coroutines.flow.Flow
import java.io.File

private val movieIdKey = ActionParameters.Key<String>(MOVIE_ID_EXTRA)

class MovieAppWidget: GlanceAppWidget() {
    override val stateDefinition = MovieGlanceStateDefinition()

    override suspend fun provideGlance(context: Context, id: GlanceId) {
        provideContent {
            val movies = currentState<List<MovieDto>>()
            GlanceTheme {
                Box(
                    modifier = GlanceModifier.background(Color(55, 0, 0))
                        .appWidgetBackground()
                        .padding(4.dp)
                ) {
                    val moviesHeader = LocalContext.current.getString(R.string.movies)
                    LazyColumn(
                        modifier = GlanceModifier
                            .fillMaxSize()
                            .padding(4.dp)
                            .background(Color(0, 55, 0))
                    ) {
                        item {
                            Text(text = moviesHeader)
                        }
                        items(items = movies) { movie ->
                            Box(
                                modifier = GlanceModifier.padding(4.dp)
                            ) {
                                Text(
                                    text = movie.title,
                                    modifier = GlanceModifier
                                        .fillMaxWidth()
                                        .padding(4.dp)
                                        .background(GlanceTheme.colors.background)
                                        .clickable(
                                            actionStartActivity<MainActivity>(
                                                actionParametersOf(
                                                    movieIdKey to movie.id
                                                )
                                            )
                                        ),
                                    style = TextStyle(
                                        fontWeight = FontWeight.Normal,
                                        fontSize = 18.sp,
                                    ),
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

class MovieGlanceStateDefinition: GlanceStateDefinition<List<MovieDto>>  {
    override suspend fun getDataStore(
        context: Context,
        fileKey: String
    ): DataStore<List<MovieDto>> = MovieDataStore(context)

    override fun getLocation(context: Context, fileKey: String): File {
        throw RuntimeException("should never be called")
    }
}

class MovieDataStore(
    context: Context,
): DataStore<List<MovieDto>> {
    val repository = MovieDatabaseRepository.create(context)
    override val data: Flow<List<MovieDto>>
        get() = repository.moviesFlow

    override suspend fun updateData(transform: suspend (t: List<MovieDto>) -> List<MovieDto>): List<MovieDto> {
        throw RuntimeException("should never be called")
    }
}