package com.androidbyexample.movies.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.repository.ActorDto

@Composable
fun ActorListUi(
    actors: List<ActorDto>,
    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedActors: () -> Unit,
    onResetDatabase: () -> Unit,
    onActorClicked: (ActorDto) -> Unit,
    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
) {
    ListScaffold(
        titleId = R.string.actors,
        items = actors,
        onItemClicked = onActorClicked,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        selectedIds = selectedIds,
        onClearSelections = onClearSelections,
        onSelectionToggle = onSelectionToggle,
        onDeleteSelectedItems = onDeleteSelectedActors,
        onResetDatabase = onResetDatabase,
    ) {actor ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Star,
                contentDescription = stringResource(id = R.string.actor),
                modifier = Modifier.clickable {
                    onSelectionToggle(actor.id)
                },
            )
            Display(text = actor.name)
        }
    }
}
