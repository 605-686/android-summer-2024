@file:OptIn(ExperimentalFoundationApi::class)

package com.androidbyexample.movies.screens

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.components.Display
import com.androidbyexample.movies.repository.MovieDto

@Composable
fun MovieListUi(
    movies: List<MovieDto>,
    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedMovies: () -> Unit,
    onResetDatabase: () -> Unit,
    onMovieClicked: (MovieDto) -> Unit,
    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,
) {
    ListScaffold(
        titleId = R.string.movies,
        items = movies,
        onItemClicked = onMovieClicked,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        selectedIds = selectedIds,
        onClearSelections = onClearSelections,
        onSelectionToggle = onSelectionToggle,
        onDeleteSelectedItems = onDeleteSelectedMovies,
        onResetDatabase = onResetDatabase,
    ) {movie ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Movie,
                contentDescription = stringResource(id = R.string.movie),
                modifier = Modifier.clickable {
                    onSelectionToggle(movie.id)
                },
            )
            Display(text = movie.title)
        }
    }
}
