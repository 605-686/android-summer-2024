# Android Summer 2024

## Videos

* Week 1: https://youtu.be/1U3fJKvHiqU
* Week 2: (Instructor sick - see https://androidbyexample.com week 2)
* Week 3: https://youtu.be/4njQvLy6bUY
* Week 4: https://youtu.be/fhdLpP7m7zo
* Week 5: (Instructor personal issue - see https://androidbyexample.com week 5. I've added MovieUi3 to the repo with the described changes)
* Week 6: https://youtu.be/YlE7GGqncS4 (Also see https://androidbyexample.com week 6, "Speech")
* Week 7: https://youtu.be/zXEZbrxg6vU
* Week 8: https://youtu.be/OjGU08NgoKU
* Week 9: https://youtu.be/zF0QUyIlgr8
* Week 10: Legacy week - see https://androidbyexample.com/modules/legacy-views/index.html.  I recommend watching at least at the Activities and Intents videos.
* Week 11: (Instructor sick - see https://androidbyexample.com/modules/testing and https://androidbyexample.com/modules/nfc)
* Week 12: https://youtu.be/Cw1n6BPJ3GY  (and https://androidbyexample.com/modules/publishing)
