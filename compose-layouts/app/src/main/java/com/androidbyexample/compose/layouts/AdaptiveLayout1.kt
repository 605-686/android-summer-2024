package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun AdaptiveLayout1(
    minWidthForSideBySide: Dp,
    modifier: Modifier,
) {
    BoxWithConstraints {
        if (maxWidth >= minWidthForSideBySide) {
            FormUsingConstraintLayout2(modifier = modifier)
        } else {
            FormUsingColumnLayout(modifier = modifier)
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun Adaptive1Preview1() {
    AdaptiveLayout1(700.dp, modifier = Modifier)
}
@Preview(
    showBackground = true,
    widthDp = 800,
    heightDp = 400,
)
@Composable
fun Adaptive1Preview2() {
    AdaptiveLayout1(700.dp, modifier = Modifier)
}