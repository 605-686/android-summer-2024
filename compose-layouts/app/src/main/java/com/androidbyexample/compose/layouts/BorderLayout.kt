package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun BorderLayout(
    modifier: Modifier = Modifier,
    north: @Composable ((Modifier) -> Unit)? = null,
    south: @Composable ((Modifier) -> Unit)? = null,
    east: @Composable ((Modifier) -> Unit)? = null,
    west: @Composable ((Modifier) -> Unit)? = null,
    center: @Composable ((Modifier) -> Unit)? = null,
) {
    Column(modifier = modifier) {
        north?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            west?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
            center?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                ) {
                    it(Modifier.fillMaxSize())
                }
            }
            east?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
        }
        south?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
    }
}
