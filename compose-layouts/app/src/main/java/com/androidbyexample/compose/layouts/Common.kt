package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstrainedLayoutReference
import androidx.constraintlayout.compose.ConstraintLayoutScope
import androidx.constraintlayout.compose.Dimension

@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier,
) {
    Text(
        text = text,
        style = MaterialTheme.typography.labelLarge,
        modifier = modifier.padding(8.dp),
    )
}

@Composable
fun Edit(
    text: String,
    modifier: Modifier = Modifier,
) {
    OutlinedTextField(
        value = text,
        textStyle = MaterialTheme.typography.titleMedium,
        onValueChange = {}, // only doing a layout example
        modifier = modifier
            .padding(8.dp)
//            .fillMaxWidth()
    )
}

@Composable
fun ConstraintLayoutScope.ConstrainedEdit(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit,
) {
    OutlinedTextField(
        value = text,
        onValueChange = {},
        textStyle = MaterialTheme.typography.titleMedium,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref) {
                constraintBlock()
                width = Dimension.fillToConstraints
            }
    )
}

@Composable
fun ConstraintLayoutScope.ConstrainedLabel(
    reference: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit,
) {
    Label(
        text = text,
        modifier = Modifier.constrainAs(reference, constraintBlock)
    )
}