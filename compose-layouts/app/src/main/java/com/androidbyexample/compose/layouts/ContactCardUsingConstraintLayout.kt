package com.androidbyexample.compose.layouts

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout

@Composable
fun ContactCardUsingConstraintLayout(modifier: Modifier) {
    ConstraintLayout(
        modifier = modifier.padding(8.dp)
    ) {
        val (circle, icon, text1, text2) = createRefs()

        createVerticalChain(text1, text2, chainStyle = ChainStyle.Packed)

        Canvas(
            modifier = Modifier
                .size(48.dp)
                .padding(8.dp)
                .constrainAs(circle) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
        ) {
            drawCircle(color = Color.Blue)
        }

        Icon(
            imageVector = Icons.Default.Person,
            contentDescription = "Person icon",
            tint = Color.White,
            modifier = Modifier
                .size(48.dp)
                .padding(12.dp)
                .constrainAs(icon) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
        )

        Text(
            text = "Scott Stanchfield",
            modifier = Modifier
                .padding(
                    start = 4.dp,
                    end = 4.dp,
                    top = 2.dp,
                    bottom = 2.dp,
                )
                .constrainAs(text1) {
                    start.linkTo(circle.end)
                }
        )
        Text(
            text = "Adjunct Instructor, JHU",
            modifier = Modifier
                .padding(
                    start = 4.dp,
                    end = 4.dp,
                    top = 2.dp,
                    bottom = 2.dp,
                )
                .constrainAs(text2) {
                    start.linkTo(circle.end)
                }
        )
    }
}

@Preview(
    showBackground = true,
    widthDp = 250,
    heightDp = 100,
)
@Composable
fun ContactCardPreview() {
    ContactCardUsingConstraintLayout(modifier = Modifier)
}
