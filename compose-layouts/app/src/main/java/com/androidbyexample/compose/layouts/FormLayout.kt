package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import kotlin.math.max

@Composable
fun FormLayout(
    minSizeForSideBySide: Dp,
    maxLabelWidth: Dp,
    modifier: Modifier,
    content: @Composable () -> Unit,
) {
    Column(modifier = modifier) {
        with(LocalDensity.current) {
            val minSizeForSideBySidePx = minSizeForSideBySide.toPx()
            val maxLabelWidthPx = maxLabelWidth.toPx()

            Layout(
                content = content,
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) { measurables, constraints ->
                // measure all measurables to get placeables

                check(measurables.size %2 == 0) { "FormLayout requires an even number of @Composables"}

                if (constraints.maxWidth < minSizeForSideBySidePx) {
                    // single-column layout
                    val measurableConstraints =
                        Constraints(
                            minWidth = constraints.minWidth, // width is based on layout
                            maxWidth = constraints.maxWidth,
                            minHeight = 0, // height is whatever it wants
                            maxHeight = Constraints.Infinity,
                        )

                    val placeables = measurables.map {
                        it.measure(measurableConstraints)
                    }

                    //                val height = max(constraints.maxHeight, placeables.sumOf { it.height })
                    val height = placeables.sumOf { it.height }

                    // then - place them
                    layout(constraints.maxWidth, height) {
                        var y = 0
                        placeables.forEach {
                            it.placeRelative(0, y)
                            y += it.height
                        }
                    }

                } else {
                    // side-by-side layout
                    val labels = measurables.filterIndexed { index, _ -> index % 2 == 0 }
                    val controls = measurables.filterIndexed { index, _ -> index % 2 == 1 }

                    val labelConstraints =
                        Constraints(
                            minWidth = 0,
                            minHeight = 0,
                            maxWidth = maxLabelWidthPx.toInt(),
                            maxHeight = Constraints.Infinity,
                        )

                    val labelPlaceables = labels.map {
                        it.measure(labelConstraints)
                    }

                    val labelsWidth = labelPlaceables.maxOf { it.width }
                    val controlsWidth = constraints.maxWidth - labelsWidth

                    val controlConstraints =
                        Constraints(
                            minWidth = controlsWidth,
                            minHeight = 0,
                            maxWidth = controlsWidth,
                            maxHeight = Constraints.Infinity,
                        )

                    val controlPlaceables = controls.map {
                        it.measure(controlConstraints)
                    }

                    val height =
                    //                    max(
                        //                        constraints.maxHeight,
                        max(
                            labelPlaceables.sumOf { it.height },
                            controlPlaceables.sumOf { it.height },
                        )
                    //                    )

                    // then - place them
                    layout(constraints.maxWidth, height) {
                        var y = 0
                        labelPlaceables.forEachIndexed { index, label ->
                            val control = controlPlaceables[index]

                            val rowHeight = max(label.height, control.height)
                            val labelOffsetY = (rowHeight - label.height) / 2
                            val controlOffsetY = (rowHeight - control.height) / 2
                            label.placeRelative(0, y + labelOffsetY)
                            control.placeRelative(labelsWidth, y + controlOffsetY)
                            y += rowHeight
                        }
                    }
                }
            }
        }
    }
}