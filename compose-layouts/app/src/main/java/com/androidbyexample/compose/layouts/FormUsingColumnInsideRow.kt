package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun FormUsingColumnInsideRow(
    modifier: Modifier
) {
    Row(modifier = modifier) {
        Column {
            Label(text = "First Name")
            Label(text = "Last Name")
            Label(text = "Age")
            Label(text = "Street")
            Label(text = "City")
            Label(text = "State")
            Label(text = "Zip Code")
        }
        Column {
            Edit("Scott")
            Edit("Stanchfield")
            Edit("56")
            Edit("123 Sesame")
            Edit("New York")
            Edit("NY")
            Edit("10101")
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun FormUCIR() {
    FormUsingColumnInsideRow(modifier = Modifier)
}