package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun FormUsingColumnLayout(
    modifier: Modifier,
) {
    Column(
        modifier = modifier.verticalScroll(rememberScrollState())
    ) {
        Label("First Name")
        Edit("Scott")
        Label("Last Name")
        Edit("Stanchfield")
        Label("Age")
        Edit("54")
        Label("Street")
        Edit("123 Sesame")
        Label("City")
        Edit("New York")
        Label("State")
        Edit("NY")
        Label("Zip")
        Edit("10101")
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun FormPreview1() {
    FormUsingColumnLayout(modifier = Modifier)
}
@Preview(
    showBackground = true,
    widthDp = 700,
    heightDp = 300,
)
@Composable
fun FormPreview2() {
    FormUsingColumnLayout(modifier = Modifier)
}