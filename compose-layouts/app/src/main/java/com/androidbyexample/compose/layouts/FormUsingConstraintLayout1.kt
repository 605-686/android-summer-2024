package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension

//data class Person(
//    val name: String,
//    val nickname: String,
//    val age: Int,
//)
//
//fun getLocation(): Pair<Int, Int> {
//    return Pair(10, 20)
//}
//    val (name, age) = Person("Scott", 57)
//
//    val (x, y) = getLocation()


@Composable
fun FormUsingConstraintLayout1(
    modifier: Modifier,
) {
    ConstraintLayout(
        modifier = modifier
            .padding(8.dp)
            .verticalScroll(rememberScrollState())
    ) {
        val (l1,l2,l3,l4,l5,l6,l7,e1,e2,e3,e4,e5,e6,e7) = createRefs()

        val barrier = createEndBarrier(l1,l2,l3,l4,l5,l6,l7)

        Label(
            text = "First Name",
            modifier = Modifier
                .constrainAs(l1) {
                    start.linkTo(parent.start)
                    top.linkTo(e1.top)
                    bottom.linkTo(e1.bottom)
                }
        )
        Label(text = "Last Name",
            modifier = Modifier
                .constrainAs(l2) {
                    start.linkTo(parent.start)
                    top.linkTo(e2.top)
                    bottom.linkTo(e2.bottom)
                }
        )
        Label(text = "Age",
            modifier = Modifier
                .constrainAs(l3) {
                    start.linkTo(parent.start)
                    top.linkTo(e3.top)
                    bottom.linkTo(e3.bottom)
                }
        )
        Label(text = "Street",
            modifier = Modifier
                .constrainAs(l4) {
                    start.linkTo(parent.start)
                    top.linkTo(e4.top)
                    bottom.linkTo(e4.bottom)
                }
        )
        Label(text = "City",
            modifier = Modifier
                .constrainAs(l5) {
                    start.linkTo(parent.start)
                    top.linkTo(e5.top)
                    bottom.linkTo(e5.bottom)
                }
        )
        Label(text = "State",
            modifier = Modifier
                .constrainAs(l6) {
                    start.linkTo(parent.start)
                    top.linkTo(e6.top)
                    bottom.linkTo(e6.bottom)
                }
        )
        Label(text = "Zip Code",
            modifier = Modifier
                .constrainAs(l7) {
                    start.linkTo(parent.start)
                    top.linkTo(e7.top)
                    bottom.linkTo(e7.bottom)
                }
        )

        Edit("Scott",
            modifier = Modifier
                .constrainAs(e1) {
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
        Edit("Stanchfield",
            modifier = Modifier
                .constrainAs(e2) {
                    end.linkTo(parent.end)
                    top.linkTo(e1.bottom)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
        Edit("56",
            modifier = Modifier
                .constrainAs(e3) {
                    end.linkTo(parent.end)
                    top.linkTo(e2.bottom)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
        Edit("123 Sesame",
            modifier = Modifier
                .constrainAs(e4) {
                    end.linkTo(parent.end)
                    top.linkTo(e3.bottom)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
        Edit("New York",
            modifier = Modifier
                .constrainAs(e5) {
                    end.linkTo(parent.end)
                    top.linkTo(e4.bottom)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
        Edit("NY",
            modifier = Modifier
                .constrainAs(e6) {
                    end.linkTo(parent.end)
                    top.linkTo(e5.bottom)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
        Edit("10101",
            modifier = Modifier
                .constrainAs(e7) {
                    end.linkTo(parent.end)
                    top.linkTo(e6.bottom)
                    start.linkTo(barrier)
                    width = Dimension.fillToConstraints
                }
        )
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun FormUCL1Preview() {
    FormUsingConstraintLayout1(modifier = Modifier)
}