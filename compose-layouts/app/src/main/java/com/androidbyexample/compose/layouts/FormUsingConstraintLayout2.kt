package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension

//data class Person(
//    val name: String,
//    val nickname: String,
//    val age: Int,
//)
//
//fun getLocation(): Pair<Int, Int> {
//    return Pair(10, 20)
//}
//    val (name, age) = Person("Scott", 57)
//
//    val (x, y) = getLocation()


@Composable
fun FormUsingConstraintLayout2(
    modifier: Modifier,
) {
    ConstraintLayout(
        modifier = modifier
            .padding(8.dp)
            .verticalScroll(rememberScrollState())
    ) {
        val (l1,l2,l3,l4,l5,l6,l7,e1,e2,e3,e4,e5,e6,e7) = createRefs()

        val barrier = createEndBarrier(l1,l2,l3,l4,l5,l6,l7)

        ConstrainedLabel(l1, "First Name") {
            start.linkTo(parent.start)
            top.linkTo(e1.top)
            bottom.linkTo(e1.bottom)
        }
        ConstrainedLabel(l2, "Last Name") {
            start.linkTo(parent.start)
            top.linkTo(e2.top)
            bottom.linkTo(e2.bottom)
        }
        ConstrainedLabel(l3, "Age") {
            start.linkTo(parent.start)
            top.linkTo(e3.top)
            bottom.linkTo(e3.bottom)
        }
        ConstrainedLabel(l4, "Street") {
            start.linkTo(parent.start)
            top.linkTo(e4.top)
            bottom.linkTo(e4.bottom)
        }
        ConstrainedLabel(l5, "City") {
            start.linkTo(parent.start)
            top.linkTo(e5.top)
            bottom.linkTo(e5.bottom)
        }
        ConstrainedLabel(l6, "State") {
            start.linkTo(parent.start)
            top.linkTo(e6.top)
            bottom.linkTo(e6.bottom)
        }
        ConstrainedLabel(l7, "Zip Code") {
            start.linkTo(parent.start)
            top.linkTo(e7.top)
            bottom.linkTo(e7.bottom)
        }

        ConstrainedEdit(e1, "Scott") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(parent.top)
        }
        ConstrainedEdit(e2, "Stanchfield") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e1.bottom)
        }
        ConstrainedEdit(e3, "56") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e2.bottom)
        }
        ConstrainedEdit(e4, "123 Sesame") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e3.bottom)
        }
        ConstrainedEdit(e5, "New York") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e4.bottom)
        }
        ConstrainedEdit(e6, "NY") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e5.bottom)
        }
        ConstrainedEdit(e7, "10101") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e6.bottom)
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun FormUCL2Preview() {
    FormUsingConstraintLayout2(modifier = Modifier)
}