package com.androidbyexample.compose.layouts

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstrainedLayoutReference
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension

//data class Person(
//    val name: String,
//    val nickname: String,
//    val age: Int,
//)
//
//fun getLocation(): Pair<Int, Int> {
//    return Pair(10, 20)
//}
//    val (name, age) = Person("Scott", 57)
//
//    val (x, y) = getLocation()


@Composable
fun FormUsingConstraintLayout3(
    modifier: Modifier,
) {
    val labels = listOf(
        "First Name",
        "Last Name",
        "Age",
        "Street",
        "City",
        "State",
        "Zip",
    )
    val values = listOf(
        "Scott",
        "Stanchfield",
        "54",
        "123 Sesame",
        "New York",
        "NY",
        "10101",
    )

    ConstraintLayout(
        modifier = modifier
            .padding(8.dp)
            .verticalScroll(rememberScrollState())
    ) {
        check(labels.size == values.size) { "Must have same number of labels and values" }

        val labelRefs = Array(labels.size) { createRef() }
        val editRefs = Array(values.size) { createRef() }

        val barrier = createEndBarrier(*labelRefs)

//        createVerticalChain(*editRefs, chainStyle = ChainStyle.Packed)

//        var lastEditRef: ConstrainedLayoutReference? = null

        labels.forEachIndexed { index, label ->
            val labelRef = labelRefs[index]
            val editRef = editRefs[index]
            val value = values[index]

            ConstrainedLabel(labelRef, label) {
                start.linkTo(parent.start)
                top.linkTo(editRef.top)
                bottom.linkTo(editRef.bottom)
            }

            ConstrainedEdit(editRef, value) {
                start.linkTo(barrier)
                end.linkTo(parent.end)
                val topLink = if (index == 0) parent.top else editRefs[index-1].bottom
                top.linkTo(topLink)
//                Log.d("!!!LAST1", lastEditRef.toString())
//                top.linkTo(lastEditRef?.bottom ?: parent.top)
            }

//            Log.d("!!!LAST2", lastEditRef.toString())
//            lastEditRef = editRef
//            Log.d("!!!LAST3", lastEditRef.toString())
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun FormUCL3Preview() {
    FormUsingConstraintLayout3(modifier = Modifier)
}