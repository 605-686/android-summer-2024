package com.androidbyexample.compose.layouts

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun FormUsingFormLayout(
    modifier: Modifier,
) {
    FormLayout(minSizeForSideBySide = 700.dp, maxLabelWidth = 200.dp, modifier = modifier) {
        Label("First Name")
        Edit("Scott")
//        Label("Last Name")
//        Edit("Stanchfield")
//        Label("Age")
//        Edit("54")
//        Label("Street")
//        Edit("123 Sesame")
        Label("City")
        Edit("New York")
        Label("State")
        Edit("NY")
        Label("Zip")
        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
    }
}

@Preview(
    showBackground = true,
    widthDp = 900,
    heightDp = 600,
)
@Composable
fun FormUFLPreview() {
    FormUsingFormLayout(modifier = Modifier)
}
