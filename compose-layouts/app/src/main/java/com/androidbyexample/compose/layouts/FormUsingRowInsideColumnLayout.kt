package com.androidbyexample.compose.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun FormUsingRowInsideColumnLayout(
    modifier: Modifier,
) {
    Column(modifier = modifier) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("First Name")
            Edit("Scott")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Last Name")
            Edit("Stanchfield")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Age")
            Edit("54")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Street")
            Edit("123 Sesame")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("City")
            Edit("New York")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("State")
            Edit("NY")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Zip")
            Edit("10101")
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 300,
    heightDp = 600,
)
@Composable
fun FormPreviewRIC1() {
    FormUsingRowInsideColumnLayout(modifier = Modifier)
}