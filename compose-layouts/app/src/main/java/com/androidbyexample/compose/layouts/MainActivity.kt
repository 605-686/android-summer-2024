package com.androidbyexample.compose.layouts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.androidbyexample.compose.layouts.ui.theme.ComposelayoutsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            ComposelayoutsTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
//                    FormUsingColumnLayout(modifier = Modifier.fillMaxSize().padding(innerPadding))
//                    FormUsingRowInsideColumnLayout(modifier = Modifier.fillMaxSize().padding(innerPadding))
//                    FormUsingConstraintLayout1(modifier = Modifier.fillMaxSize().padding(innerPadding))
//                    FormUsingConstraintLayout2(modifier = Modifier.fillMaxSize().padding(innerPadding))
//                    FormUsingConstraintLayout3(modifier = Modifier.fillMaxSize().padding(innerPadding))
//                    AdaptiveLayout1(minWidthForSideBySide = 700.dp, modifier = Modifier.fillMaxSize().padding(innerPadding))
                    FormUsingFormLayout(modifier = Modifier.fillMaxSize().padding(innerPadding))
                }
            }
        }
    }
}
