package com.androidbyexample.compose.layouts

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun SampleBorderLayout(
    modifier: Modifier
) {
    BorderLayout(
        modifier = modifier,
        north = { Text("NORTH", modifier = it.background(Color.Blue)) },
        south = { Text("SOUTH", modifier = it.background(Color.Blue)) },
        east = { Text("EAST", modifier = it.background(Color.Green)) },
        west = { Text("WEST", modifier = it.background(Color.Green)) },
        center = { Text("CENTER", modifier = it.background(Color.Red)) },
    )
}

@Preview(
    showBackground = true,
    widthDp = 400,
    heightDp = 400,
)
@Composable
fun BorderLayoutPreview() {
    SampleBorderLayout(modifier = Modifier)
}